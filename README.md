# Minecraft RL

Reinforcement learning agent to solve the MineRL environment.
Some helpful links: <br>
* [Competition AICrowd](https://www.aicrowd.com/challenges/neurips-2019-minerl-competition "AICrowd MineRL Competition"),
* [MineRL Documentation](http://www.minerl.io/docs/ "MineRL Docs"),
* [MineRL GitHub](https://github.com/minerllabs/minerl "MineRL GitHub"),
* [Starter baselines](https://github.com/minerllabs/quick_start/tree/master/chainerrl_baselines "MineRL GitHub").

To run headless, use: `xvfb-run python3 main.py`. 
Note that this uses GPU memory.

Remember to keep the [trello board](https://trello.com/b/VfFUlc5C/minerl "Trello MineRL") updated.

