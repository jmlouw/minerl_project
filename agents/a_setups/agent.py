import torch
import numpy as np
from utils.gen_args import Arguments


class Agent(object):
    def __init__(self):
        pass

    def transform_action_space(self, action_space, args):
        raise NotImplemented

    def get_actor_model(self, my_action_space):
        raise NotImplemented

    def get_critic(self, my_action_space):
        raise NotImplemented

    def cam_accuracy(self, net_output, action_label):
        raise NotImplemented

    def act_accuracy(self,net_output, action_label):
        action_label = action_label.long()
        return action_label, torch.mean((net_output.argmax(1) == action_label).float()).detach().cpu().numpy()

    def wrap_environment(self, env, args: Arguments):
        raise NotImplemented
    
    def get_loss_functions(self, my_action_space):
        raise NotImplemented

    def convert_to_action(self, output, env):
        raise NotImplemented

    def insert_in_actiondict(self, outputs, env):
        raise NotImplemented

    @staticmethod
    def _extract_inv(obs):
        if 'inventory' in obs.keys():  # Get inventory data, concatenate and convert to tensor
            inv = np.stack(list(obs['inventory'].values()))
            mainhand = obs['equipped_items']['mainhand']['type']
            inv = np.concatenate(([mainhand if not isinstance(mainhand, str) else 8], inv))
            # inv[1:] = inv[1:] / INV_NORM
        else:
            inv = np.zeros((19,))
        return torch.from_numpy(inv).float()