from agents.a_setups.agent import Agent
from utils.gen_args import Arguments
from utils.wrappers import actions_, ActionSpace_
from agents.model import Actor_, Critic_
import numpy as np
import torch.nn as nn
import torch

class Agent_(Agent):
    def __init__(self):
        super().__init__()
        #self.env = CombinedActionsDiscrete(env, args.cam_bins)

    def transform_action_space(self, action_space, args):
        self.my_action_space = actions_(action_space, args)
        return self.my_action_space

    def get_actor_model(self, my_action_space):
        return Actor_(my_action_space)

    def act_accuracy(self,net_output, action_label):
        action_label = action_label.long()
        #print(action_label)
        #print((len(net_output)-2))
        #print(action_label, net_output.argmax(1))
        return action_label, torch.mean((net_output.argmax(1) == action_label).float()).detach().cpu().numpy()

    def cam_accuracy(self, net_output, action_label):
        action_label = action_label.long()
        net_output = net_output.squeeze()
        return action_label, torch.mean((net_output.argmax(1) == action_label).float()).detach().cpu().numpy()

    def wrap_environment(self, env, args: Arguments):
        return ActionSpace_(env, args)

    def get_loss_functions(self, my_action_space):
        return nn.CrossEntropyLoss()

    def convert_to_action(self, output, env): #for pretraining
        action = env.action_space.no_op()
        action['action'] = output.argmax(1)
        return action

    def insert_in_actiondict(self, output, env): #for online training
        action = env.action_space.no_op()
        action['action'] = output
        return action

    def get_critic(self, my_action_space):
        return Critic_(my_action_space)
