from agents.a_setups.agent import Agent
from utils.gen_args import Arguments
from utils.wrappers import branched_actions, BranchedActionSpace
from agents.model import Actor_Branched, Critic_Branched
import numpy as np
import torch.nn as nn
import torch

class Agent_Branched(Agent):
    def __init__(self):
        super().__init__()
        #self.env = CombinedActionsDiscrete(env, args.cam_bins)

    def transform_action_space(self, action_space, args):
        self.my_action_space = branched_actions(action_space, args)
        return self.my_action_space

    def get_actor_model(self, my_action_space):
        return Actor_Branched(my_action_space)

    def act_accuracy(self,net_output, action_label):
        action_label = action_label.long()
        #print(action_label)
        #print((len(net_output)-2))
        #print(action_label, net_output.argmax(1))
        return action_label, torch.mean((net_output.argmax(1) == action_label).float()).detach().cpu().numpy()

    def cam_accuracy(self, net_output, action_label):
        action_label = action_label.long()
        net_output = net_output.squeeze()
        return action_label, torch.mean((net_output.argmax(1) == action_label).float()).detach().cpu().numpy()

    def wrap_environment(self, env, args: Arguments):
        return BranchedActionSpace(env, args)

    def get_loss_functions(self, my_action_space):
        loss_functions = []
        for i in range(len(my_action_space.spaces)):
            loss_functions.append(nn.CrossEntropyLoss())
        return loss_functions

    def convert_to_action(self, outputs, env):
        action = env.action_space.no_op()
        #print(outputs)
        for key, output in zip(env.action_space.spaces.keys(), outputs):
            action[key] = output.argmax(1)

        return action

    def insert_in_actiondict(self, outputs, env):
        action = env.action_space.no_op()
        #print((outputs))
        for key, output in zip(action.keys(), outputs):
            action[key] = output.item()
        action['attack'] = 1
        return action

    def get_critic(self, my_action_space):
        return Critic_Branched(my_action_space)
