from agents.a_setups.agent import Agent
from utils.gen_args import Arguments
from utils.wrappers import CombinedActionsCont, combine_actions_cont
from agents.model import Actor_Cont
import torch.nn as nn
import torch

class Agent_Cont_Cam(Agent):
    def __init__(self ):
        super().__init__()

    def transform_action_space(self, action_space, args):
        return combine_actions_cont(action_space)

    def get_actor_model(self, my_action_space):
        return Actor_Cont(my_action_space)

    def cam_accuracy(self, net_output, action_label):
        net_output = net_output.squeeze()
        return action_label, torch.mean(torch.abs(action_label - net_output)).detach().cpu().numpy() / 2

    def wrap_environment(self, env, args: Arguments):
        return CombinedActionsCont(env)

    def get_loss_functions(self, my_action_space):
        return [nn.SmoothL1Loss(), nn.SmoothL1Loss(), nn.CrossEntropyLoss()]

