from agents.a_setups.agent import Agent
from utils.gen_args import Arguments
from utils.wrappers import CombinedActionsDiscrete, combine_actions_discrete
from agents.model import Actor_Discrete
import torch.nn as nn
import torch

class Agent_Disc_Cam(Agent):
    def __init__(self):
        super().__init__()

    def transform_action_space(self, action_space, args):
        return combine_actions_discrete(action_space, args)

    def get_actor_model(self, my_action_space):
        return Actor_Discrete(my_action_space)

    def cam_accuracy(self, net_output, action_label):
        action_label = action_label.long()
        net_output = net_output.squeeze()
        return action_label, torch.mean((net_output.argmax(1) == action_label).float()).detach().cpu().numpy() / 2

    def wrap_environment(self, env, args: Arguments):
        return CombinedActionsDiscrete(env, args)

    def get_loss_functions(self, my_action_space):
        return [nn.CrossEntropyLoss(), nn.CrossEntropyLoss(), nn.CrossEntropyLoss()]