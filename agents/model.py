import torch
import torch.nn as nn
from torch.distributions import Normal, Categorical

class GradScalar(torch.autograd.Function):
    """
    We can implement our own custom autograd Functions by subclassing
    torch.autograd.Function and implementing the forward and backward passes
    which operate on Tensors.
    """

    @staticmethod
    def forward(ctx, input, nr_branches):
        """
        In the forward pass we receive a Tensor containing the input and return
        a Tensor containing the output. ctx is a context object that can be used
        to stash information for backward computation. You can cache arbitrary
        objects for use in the backward pass using the ctx.save_for_backward method.
        """
        ctx.save_for_backward(nr_branches)
        return input

    @staticmethod
    def backward(ctx, grad_output):
        """
        In the backward pass we receive a Tensor containing the gradient of the loss
        with respect to the output, and we need to compute the gradient of the loss
        with respect to the input.
        """
        nr_branches, = ctx.saved_tensors
        grad_input = grad_output.clone()
        grad_input = grad_input / nr_branches
        return grad_input, None

class ScaleGrads(nn.Module):
    def __init__(self, nr_branches):
        super(ScaleGrads, self).__init__()
        self.nr_branches = torch.tensor(nr_branches, dtype=torch.float)

    def forward(self, input):
        return GradScalar.apply(input, self.nr_branches)

class Flatten(nn.Module):
    def forward(self, input):
        return input.view(input.size(0), -1)

def init_weights(m):
    if isinstance(m, nn.Linear):
        nn.init.normal(m.weight, mean=0., std=0.1)
        nn.init.constant_(m.bias, 0.1)

## Neural network blocks ##
def cnn_block():
    return nn.Sequential(
        nn.Conv2d(in_channels=3, out_channels=32, kernel_size=8, stride=4),
        nn.ReLU(),
        nn.Conv2d(in_channels=32, out_channels=64, kernel_size=4, stride=2),
        nn.ReLU(),
        nn.Conv2d(in_channels=64, out_channels=64, kernel_size=3, stride=1),
        nn.ReLU(),
        Flatten(),
    )

def inventory_block(no_inputs=19, no_outputs=64):
    return nn.Sequential(
        nn.Linear(in_features=no_inputs, out_features=no_outputs),
        nn.ReLU()
    )

def camera_block(no_inputs):
    return nn.Sequential(
        nn.Linear(in_features=no_inputs, out_features=150),
        nn.ReLU(),
        nn.Linear(in_features=150, out_features=1),
        nn.Tanh()  # e of (-1, 1) -> scale to (-10, 10)
    )

def act_block(no_inputs, no_outputs):
    return nn.Sequential(
        nn.Linear(in_features=no_inputs, out_features=150),
        nn.ReLU(),
        nn.Linear(in_features=150, out_features=no_outputs),
    )

class Actor(nn.Module):
    def __init__(self, action_space, std_pitch=0.0, std_yaw=0.0):
        super(Actor, self).__init__()
        self.main_outputs = 500

        self.cnn = cnn_block(no_outputs=self.main_outputs, no_branches=3)
        self.c_yaw_block = act_block(no_inputs=self.main_outputs, no_outputs=action_space['yaw'].n)
        self.c_pitch_block = act_block(no_inputs=self.main_outputs, no_outputs=action_space['pitch'].n)
        self.act_block = act_block(no_inputs=self.main_outputs, no_outputs=action_space['act'].n)

        self.item_block = None
        if 'item' in action_space.spaces:
            self.item_block = act_block(no_inputs=self.main_outputs, no_outputs=action_space['item'].n)

        self.log_std_pitch = nn.Parameter(torch.tensor(std_pitch))
        self.log_std_yaw = nn.Parameter(torch.tensor((std_yaw)))

    def forward(self, t):
        t = self.cnn(t)
        act = self.act_block(t)
        pitch = self.c_pitch_block(t)
        yaw = self.c_yaw_block(t)
        if self.item_block is not None:
            item = self.item_block(t)
            return pitch, yaw, act, item
        return pitch, yaw, act

class Actor_sep(Actor):  # continuous
    def __init__(self, action_space, std_pitch=0.0, std_yaw=0.0):
        super(Actor_sep, self).__init__(action_space, std_pitch=0.0, std_yaw=0.0)
        self.main_block_camera = cnn_block(no_outputs=self.main_outputs, no_branches=2)
        self.pitch_block = camera_block(no_inputs=self.main_outputs)
        self.yaw_block = camera_block(no_inputs=self.main_outputs)

        self.main_block_act = cnn_block(no_outputs=self.main_outputs, no_branches=1)
        self.act_block = act_block(no_inputs=self.main_outputs, no_outputs=action_space['act'].n)

    def forward(self, t):
        main_cam = self.main_block_camera(t)
        main_act = self.main_block_act(t)

        act = self.act_block(main_act)

        pitch = self.pitch_block(main_cam) * 10
        yaw = self.yaw_block(main_cam) * 10
        if self.item_block is not None:
            item = self.item_block(main_act)
            return pitch, yaw, act, item
        return pitch, yaw, act

class Actor_Discrete(Actor):
    def __init__(self, action_space, std_pitch=0.0, std_yaw=0.0):
        super(Actor_Discrete, self).__init__(action_space, std_pitch=0.0, std_yaw=0.0)

class Actor_Cont(Actor):
    def __init__(self, action_space, std_pitch=0.0, std_yaw=0.0):
        super(Actor_Cont, self).__init__(action_space, std_pitch=0.0, std_yaw=0.0)

        self.c_yaw_block = camera_block(no_inputs=self.main_outputs)
        self.c_pitch_block = camera_block(no_inputs=self.main_outputs)

    def forward(self, t):
        t = self.cnn(t)
        act = self.act_block(t)
        pitch = self.c_pitch_block(t) * 10
        yaw = self.c_yaw_block(t) * 10
        if self.item_block is not None:
            item = self.item_block(t)
            return pitch, yaw, act, item
        return pitch, yaw, act

    def act(self, state, memory, device):
        state = state.to(device)
        action_probs = self.cnn(state)

        action_mean_pitch = self.c_pitch_block(action_probs)
        action_mean_yaw = self.c_yaw_block(action_probs)
        action_probs_act = self.act_block(action_probs)

        dist_pitch = Normal(action_mean_pitch, self.log_std_pitch.exp().expand_as(action_mean_pitch))
        dist_yaw = Normal(action_mean_yaw, self.log_std_yaw.exp().expand_as(action_mean_yaw))
        dist_act = Categorical(action_probs_act)

        dists = (dist_pitch, dist_yaw, dist_act)

        actions = []

        for dist, memory_action, memory_logprobs in zip(dists, memory.actions, memory.log_probs):
            action = dist.sample()
            actions.append(action)
            memory_action.append(action)
            memory_logprobs.append(dist.log_prob(action))

        memory.states.append(state)
        return actions

    def evaluate(self, state, memory_actions):
        action_pitch, action_yaw, action_act = memory_actions

        t = self.cnn(state)
        action_probs = self.cnn(state)

        action_mean_pitch = self.c_pitch_block(action_probs)
        action_mean_yaw = self.c_yaw_block(action_probs)
        action_probs_act = self.act_block(action_probs)

        dist_pitch = Normal(action_mean_pitch, self.log_std_pitch.exp().expand_as(action_mean_pitch))
        dist_yaw = Normal(action_mean_yaw, self.log_std_yaw.exp().expand_as(action_mean_yaw))
        dist_act = Categorical(action_probs_act)

        log_probs = (dist_pitch.log_prob(action_pitch), dist_yaw.log_prob(action_yaw), dist_act.log_prob(action_act))

        entropies = (dist_pitch.entropy(), dist_yaw.entropy(), dist_act.entropy())

        return log_probs, entropies

class Actor_Branched(nn.Module):
    def __init__(self, my_action_space, std_pitch=0.0, std_yaw=0.0):
        super(Actor_Branched, self).__init__()
        self.main_outputs = 1000

        self.cnn = cnn_block()
        self.inv_layer = inventory_block(no_inputs=19, no_outputs=64)

        self.prebranch = nn.Linear(in_features=4 * 4 * 64 + 64, out_features=self.main_outputs)
        self.scaler = ScaleGrads(nr_branches=len(my_action_space.spaces))

        # self.camera_main = act_block(no_inputs=self.main_outputs, no_outputs=500)
        self.branches = nn.ModuleList()
        self.softmax = nn.Softmax(dim=-1)
        self.ReLU = nn.ReLU()
        for count, (branch_key, branch_value) in enumerate(my_action_space.spaces.items()):
            self.branches.append(act_block(no_inputs=self.main_outputs, no_outputs=branch_value.n))

    def forward(self, t, inv = None):
        if inv is not None:
           inv = self.inv_layer(inv)
        else:
            inv = self.inv_layer(torch.zeros((t.shape[0], 19)).to(t))

        t = self.cnn(t)
        t = torch.cat((t, inv), -1)
        t = self.ReLU(self.prebranch(t))
        t = self.scaler(t)
        # cm = self.camera_main(t)
        outputs = []
        for count, branch in enumerate(self.branches):
            outputs.append(branch(t))
        return outputs

    def act(self, t, inv = None, memory=None):
        # print("before cnn ", t)
        # print('before inv layer:', inv)
        # state = state.to(device)
        if inv is not None:
            inv = self.inv_layer(inv)
        else:
            inv = self.inv_layer(torch.zeros((t.shape[0], 19)).to(t))
        # print('inv_layer', inv)
        t = self.cnn(t)
        # print('After cnn', t)
        t = torch.cat((t, inv), -1)
        # print(f'After cat: {t}')
        t = self.ReLU(self.prebranch(t))
        # print(f'After relu: {t}')
        t = self.scaler(t)
        # print(f'After scaler: {t}')
        # cm = self.camera_main(t)
        actions = []

        for count, branch in enumerate(self.branches):
            #print(branch(t))
            #print(self.softmax(branch(t)))
            dist = Categorical(self.softmax(branch(t)))
            action = dist.sample()
            actions.append(action)

            if memory is not None:
                memory.actions[count].append(action)
                memory.logprobs[count].append(dist.log_prob(action))

        #if memory is not None:

        return actions

    def evaluate(self, t, actions, device, inv = None):
        if inv is not None:
            inv = self.inv_layer(inv)
        else:
            inv = self.inv_layer(torch.zeros((t.shape[0], 19)).to(t))

        t = self.cnn(t)
        t = torch.cat((t, inv), -1)
        t = self.ReLU(self.prebranch(t))
        t = self.scaler(t)

        action_logprobs = []
        dist_entropies = []

        for branch, actions_br in zip(self.branches, actions):
            actions_br_ten = torch.stack(actions_br).to(device).detach()
            dist = Categorical(self.softmax(branch(t)))
            action_logprobs.append(dist.log_prob(actions_br_ten))
            dist_entropies.append(dist.entropy())

        return action_logprobs, dist_entropies

class Critic_Branched(nn.Module):
    def __init__(self, my_action_space):
        super(Critic_Branched, self).__init__()

        self.main_outputs = 1000
        self.cnn = cnn_block()
        self.inv_layer = inventory_block(no_inputs=19, no_outputs=64)

        self.prebranch = nn.Linear(in_features=4 * 4 * 64 + 64, out_features=self.main_outputs)
        self.scaler = ScaleGrads(nr_branches=len(my_action_space.spaces))
        self.branches = nn.ModuleList()

        for i in range(len(my_action_space.spaces)):
            self.branches.append(act_block(no_inputs=self.main_outputs, no_outputs=1))

    def forward(self, t, inv=None):
        if inv is not None:
            inv = self.inv_layer(inv)
        else:
            inv = self.inv_layer(torch.zeros((t.shape[0], 19)).to(t))

        t = self.cnn(t)
        t = torch.cat((t, inv), -1)
        t = nn.ReLU()(self.prebranch(t))
        t = self.scaler(t)
        values = []
        for count, branch in enumerate(self.branches):
            values.append(nn.ReLU()(branch(t)))

        return values

class Actor_(nn.Module):
    def __init__(self, my_action_space):
        super(Actor_, self).__init__()
        print(my_action_space)
        self.main_outputs = 1000

        self.cnn = cnn_block()
        self.inv_layer = inventory_block(no_inputs=19, no_outputs=64)

        self.preact = nn.Linear(in_features=4 * 4 * 64 + 64, out_features=self.main_outputs)

        self.softmax = nn.Softmax(dim=-1)
        self.ReLU = nn.ReLU()

        self.actblock = act_block(no_inputs=self.main_outputs, no_outputs=my_action_space['action'].n)

    def act(self, t, inv = None, memory=None):
        if inv is not None:
            inv = self.inv_layer(inv)
        else:
            inv = self.inv_layer(torch.zeros((t.shape[0], 19)).to(t))

        t = self.cnn(t)
        t = torch.cat((t, inv), -1)
        t = self.ReLU(self.preact(t))

        dist = Categorical(self.softmax(self.actblock(t)))
        action = dist.sample()

        if memory is not None:
            memory.actions.append(action)
            memory.logprobs.append(dist.log_prob(action))

        return action.item()

    def evaluate(self, t, action, device, inv = None):
        if inv is not None:
            inv = self.inv_layer(inv)
        else:
            inv = self.inv_layer(torch.zeros((t.shape[0], 19)).to(t))

        t = self.cnn(t)
        t = torch.cat((t, inv), -1)
        t = self.ReLU(self.preact(t))

        dist = Categorical(self.softmax(self.actblock(t)))
        action_logprobs = dist.log_prob(action)
        dist_entropy = dist.entropy()

        return action_logprobs, dist_entropy

class Critic_(nn.Module):
    def __init__(self, my_action_space):
        super(Critic_, self).__init__()

        self.main_outputs = 1000
        self.cnn = cnn_block()
        self.inv_layer = inventory_block(no_inputs=19, no_outputs=64)

        self.prevalue = nn.Linear(in_features=4 * 4 * 64 + 64, out_features=self.main_outputs)

        self.ReLU = nn.ReLU()
        self.valueblock = act_block(no_inputs=self.main_outputs, no_outputs=1)


    def forward(self,  t, inv=None):
        if inv is not None:
            inv = self.inv_layer(inv)
        else:
            inv = self.inv_layer(torch.zeros((t.shape[0], 19)).to(t))

        t = self.cnn(t)
        t = torch.cat((t, inv), -1)
        t = nn.ReLU()(self.prevalue(t))

        value = self.valueblock(t)

        return value