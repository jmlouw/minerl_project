import copy
import os
import torch
import torch.nn as nn
import torch.optim as optim
import gym
import minerl
from pathlib import Path
from tqdm import tqdm
import numpy as np
from torch.utils import tensorboard
import collections

# user imports
from utils.gen_args import Arguments
from utils.utility_functions import print_b, to_tensor
from agents.a_setups.agent import Agent
from utils.util import checkpoint_load_actor, checkpoint_save


class Memory:
    def __init__(self, my_action_space):
        self.no_branches = len(my_action_space.spaces)
        self.actions = [[] for _ in range(self.no_branches)]  # (no_branches, seq_siz)
        self.states = {'obs': [], 'invs': []}
        self.logprobs = [[] for _ in range(self.no_branches)]
        self.rewards = []

    def clear_memory(self):
        self.actions = [[] for _ in range(self.no_branches)]
        #del self.actions[:]
        self.states = {'obs': [], 'invs': []}
        self.logprobs = [[] for _ in range(self.no_branches)]
        #del self.logprobs[:]
        del self.rewards[:]


class PPOAgent(object):
    def __init__(self, env, agent: Agent, args: Arguments):
        self.eps_clip = args.eps_clip
        self.batch_size = args.batch_size
        self.gamma = args.gamma
        self.K_epochs = args.K_epochs

        self.actor = agent.get_actor_model(env.action_space).to(args.device)  # create the neural network
        self.critic = agent.get_critic(env.action_space).to(args.device)

        self.actor_optim = torch.optim.Adam(self.actor.parameters(), lr=args.lr)
        self.critic_optim= torch.optim.Adam(self.critic.parameters(), lr=args.lr)

        if os.path.exists(args.model_path):
            checkpoint_load_actor(self.actor, args.model_path)

        self.actor_old = copy.deepcopy(self.actor)
        self.mseLoss = nn.SmoothL1Loss()

    def agent_main(self, env, agent: Agent, args: Arguments):
        """
        Training main method
        :return:
        """
        # Create environment
        memory = Memory(env.action_space)
        update_timestep = 1000
        max_env_steps = 8000000
        render = True

        # logging variables
        ep_reward = 0
        ep_no = 1
        
        timestep = 0
        # training loop
        obs = env.reset()
        print('Episode: ', 1)
        for step in range(1, max_env_steps + 1):
            timestep += 1

            # Running policy_old:
            inv = agent._extract_inv(obs).unsqueeze(0)
            obs = to_tensor(obs['pov'].copy()).unsqueeze(0)
            #print(inv)
            memory.states['invs'].append(inv)
            memory.states['obs'].append(obs)

            inv = inv.to(args.device)
            obs = obs.to(args.device)

            #print(inv)
            #print(obs)

            output = self.actor_old.act(obs, inv, memory)
            action = agent.insert_in_actiondict(output, env)

            obs, reward, done, _ = env.step(action)
            ep_reward += reward

            if reward > 0:
                print(reward, sep=' ', end='', flush=True)
            # Saving reward
            memory.rewards.append(reward)

            # update if its time
            if timestep % update_timestep == 0:
                self.update(memory, args)
                memory.clear_memory()

            
            if render:
                env.render()

            if done:
                print('\nreward: {}'.format(ep_reward))
                ep_reward = 0
                ep_no += 1
                print('Episode: ', ep_no)
                obs = env.reset()

            if timestep % 35000 == 0:
                checkpoint_save({
                    'actor': self.actor.state_dict(),
                    'optimizer_actor': self.actor_optim.state_dict(),
                    'critic': self.critic.state_dict(),
                    'optimizer_critic': self.critic_optim.state_dict(),
                    'step': step + 1
                }, args.model_path)

    def update(self, memory, args: Arguments):
        # Monte Carlo estimate of state rewards:
        rewards = []
        discounted_reward = 0
        for reward in reversed(memory.rewards):
            discounted_reward = reward + (self.gamma * discounted_reward)
            rewards.insert(0, discounted_reward)

        # Normalizing the rewards:
        rewards = torch.tensor(rewards).to(args.device)
        rewards = (rewards - rewards.mean()) / (rewards.std() + 1e-5)

        # convert list to tensor
        old_obs = torch.cat(memory.states['obs']).to(args.device).detach()
        old_invs = torch.cat(memory.states['invs']).to(args.device).detach()

        # Optimize policy for K epochs:
        for _ in range(self.K_epochs):
            # Evaluating old actions and values:
            logprobs, dist_entropies = self.actor.evaluate(old_obs, memory.actions, args.device, old_invs)
            state_values = self.critic(old_obs)
            loss = 0.0

            for logprobs_br, entropies_br, state_values_br, old_logprobs_br in \
                    zip(logprobs, dist_entropies, state_values, memory.logprobs): #loop for each branch
                old_logprobs_br = torch.stack(old_logprobs_br).to(args.device).detach()
                state_values_br = state_values_br.squeeze()

                # Finding the ratio (pi_theta / pi_theta_old):
                ratios = torch.exp(logprobs_br - old_logprobs_br.detach())

                # Finding Surrogate Loss:
                advantages = rewards - state_values_br.detach()
                surr1 = ratios * advantages
                surr2 = torch.clamp(ratios, 1 - self.eps_clip, 1 + self.eps_clip) * advantages
                loss += -torch.min(surr1, surr2) + 0.5*self.mseLoss(state_values_br, rewards) - 0.01*entropies_br

            # take gradient step
            self.actor_optim.zero_grad()
            self.critic_optim.zero_grad()
            loss.mean().backward()
            self.actor_optim.step()
            self.critic_optim.step()

        # Copy new weights into old policy
        self.actor_old.load_state_dict(self.actor.state_dict())
