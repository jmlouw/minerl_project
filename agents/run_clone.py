import itertools
import os
from pathlib import Path
import copy
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from collections import namedtuple, OrderedDict, deque
import gc
import gym
import minerl

# user imports
from agents.model import Actor_Cont, Actor_sep, Critic_Branched, Actor_Discrete
from agents.a_setups.agent import Agent
from agents.a_setups.agent_disc_cam import Agent_Disc_Cam
from agents.test_agent import TestAgent
from utils.gen_args import Arguments
from utils.utility_functions import to_tensor
from utils.util import checkpoint_save, checkpoint_load
from utils.wrappers import combine_actions_cont, combine_actions_discrete, ActionConverter

LR = 1e-4
BATCH_SIZE = 64
HORIZON = 64
EPSILON = 0.0
EPOCHS = -1
CAPACITY = 400000
EVALUATE_STEP = 5000
Transition = namedtuple('Transition', 'state action reward done, inv')
#loss_functions = [nn.SmoothL1Loss(), nn.SmoothL1Loss(), nn.CrossEntropyLoss()]

class Buffer:
    def __init__(self, capacity):
        self.data = [None] * capacity
        self.index = 0
        self.capacity = capacity

    def append(self, transition):
        self.index %= self.capacity
        self.data[self.index] = transition
        self.index += 1
        #print(self.index)

    @property
    def full(self):
        # print(self.data[-1])
        #print(self.data[-1] is not None)
        return self.data[-1] is not None

    def sample(self, size):
        """
        {
            pitch: 1
            yaw: 2
            btn: 3
        }*3

        [[1,2,3],[1,2,3],[1,2,3]]
        """
        indices = np.random.choice(self.capacity, size)
        samples = [self.data[idx] for idx in indices]
        #print(samples)
        states = [sample.state for sample in samples]
        actions = [sample.action for sample in samples]  # [dict{}, dict, dict, dict] -> [[], [], []] shape = (64, 3).transpose -> (3, 64)
        actions = torch.t(torch.tensor([list(action.values()) for action in actions], dtype=torch.float32))
        #print(actions.shape)
        rewards = [sample.reward for sample in samples]
        dones = [sample.done for sample in samples]
        invs = [sample.inv for sample in samples]
        return (
            states, actions, rewards, dones, invs
        )


def main(args: Arguments, agent: Agent, env, data_name):
    print('Data Environment: ', data_name)
    print('Model Path: ', args.model_path)

    training_data = read_dataset(
        'minerl_data_set', data_name, num_workers=8)
    #action_space = combine_actions(training_data.action_space)

    my_action_space = agent.transform_action_space(training_data.action_space, args)
    loss_functions = agent.get_loss_functions(my_action_space)

    buffer = load_buffer(training_data, my_action_space, args)

    model_actor = agent.get_actor_model(my_action_space)
    model_critic = Critic_Branched(my_action_space)
    optim_actor = optim.Adam(model_actor.parameters(), lr=LR)
    optim_critic = optim.Adam(model_critic.parameters(), lr=LR)

    #step_start = 0

    if os.path.exists(args.model_path) and args.restore:
        step_start = checkpoint_load(model_actor, optim_actor, model_critic, optim_critic, True, args.model_path, device = args.device)
    step_start = 0
    model_actor = model_actor.to(args.device)

    # TRAINING LOOP
    btn_acc = 0.0
    cam_acc = 0.0
    step = 0
    for step in range(step_start, args.pretrain_steps):

        states, action_labels, rewards, dones, invs = buffer.sample(BATCH_SIZE)
        #print(action_labels)
        input_net = to_tensor(states).to(args.device)
        invs = torch.tensor(invs).float().to(args.device)
        action_labels = action_labels.to(args.device)
        outputs = model_actor(input_net, invs)

        #print('NET OUTPUTS:', outputs)

        loss = 0.0

        for count, (net_output, action_label, loss_function) in enumerate(zip(outputs, action_labels, loss_functions)):
            #print("Action Labels!")
           # print(len(action_label))
            if count < 2:
                net_output = net_output.squeeze()
                action_label, cur_cam = agent.cam_accuracy(net_output, action_label)
                cam_acc += cur_cam / 2
            else:
                action_label, cur_act = agent.act_accuracy(net_output, action_label)
                btn_acc += cur_act  / (len(outputs)-2)

            loss += loss_function(net_output, action_label)

        #btn_acc /= 3
        optim_actor.zero_grad()
        loss.backward()
        optim_actor.step()

        if (step+1) % 1000 == 0:
            #roots = objgraph.get_leaking_objects()
            #objgraph.show_most_common_types(objects=roots)
            #tracker.print_diff()
            btn_acc /= 1000
            cam_acc /= 1000
            print('step: ', step+1)
            print(f'Camera Acc: {cam_acc*100:.5f}')
            print(f'Btn Acc: {btn_acc*100:.2f} %\n')
            checkpoint_save({
                'actor': model_actor.state_dict(),
                'optimizer_actor': optim_actor.state_dict(),
                'critic': model_critic.state_dict(),
                'optimizer_critic': optim_critic.state_dict(),
                'step': step +1
            }, args.model_path)

            btn_acc = 0.0
            cam_acc = 0.0
            #gc.collect()

        if (step + 1) % EVALUATE_STEP == 0 and (args.testing == True):
            print('Testing Agent:')
            TestAgent.agent_play(model_actor, args, env, agent)


def load_buffer(training_data, my_action_space, args):
    buffer = Buffer(CAPACITY)

    print("Loading buffer...")# ek glo ds fine... was net weird... as ek die exclude actions vroeer in daardie blok geroep het, het ek n NoneType error gekry, maar lyk of dit nou werk
    # Wag gou
    empty_action = my_action_space.no_op()

    for current_states, actions, rewards, next_states, dones in training_data.sarsd_iter(
            num_epochs=EPOCHS, max_sequence_len=-1):

        seq_size = len(actions['attack'])
        ActionConverter.generate_labels_branched(actions, discrete_cam=isinstance(my_action_space['pitch'], minerl.env.spaces.Discrete), args=args)
        #print(actions)
        new_actions = [copy.deepcopy(empty_action) for _ in range(seq_size)]
        #print(new_actions)
        for key, values in actions.items():
            for x in range(seq_size):
                new_actions[x][key] = values[x]

        actions = new_actions
        #print(actions)
        #print("\n")
        invs = np.zeros((seq_size, 19))
        if 'Obtain' in args.env_name:
            invs = np.stack(list(current_states['inventory'].values()))
            invs = np.concatenate(([current_states['equipped_items']['mainhand']['type']], invs)).transpose()

        for current_state, action, reward, done, inv in zip(current_states['pov'], actions, rewards, dones, invs):

            buffer.append(Transition(current_state, action, reward, done, inv))
            if buffer.full:
                training_data.processing_pool.close()
                training_data.processing_pool.terminate()
                break
        else:
            continue
        break
    return buffer

def read_dataset(data_folder: str, demo_set: str, num_workers: int):
    data_folder = Path('/'.join([str(Path(__file__).parents[2]), data_folder]))
    return minerl.data.make(demo_set, data_dir=data_folder, num_workers=num_workers)

