import gym
import minerl
import os
import torch
import numpy as np
import matplotlib.pyplot as plt
from gym.wrappers import Monitor
from collections import OrderedDict

from agents.a_setups.agent import Agent
from utils.wrappers import CombinedActionsCont, CombinedActionsDiscrete
# user imports
from agents.model import Actor_Cont, Actor_sep, Actor_Discrete
from utils.util import checkpoint_load_actor
from utils.gen_args import Arguments
from utils.utility_functions import to_tensor
from utils.terminal_image import show_image


class TestAgent:
    def __init__(self, env, agent: Agent, args: Arguments):
        self.args = args

        # create cuda device

        # create network
        #self.env = agent.wrap_environment(env, args)
        model_actor = agent.get_actor_model(env.action_space)

        # try to restore the network from the data file
        print(args.model_path)
        if  os.path.exists(args.model_path):

            checkpoint_load_actor(model_actor, args.model_path)


        env.seed(300)
        self.agent_play(model_actor, args, env, agent)
        #self.env.reset()
       # self.agent_play(model_actor, dev)

    @staticmethod
    def agent_play(model, args, env, agent):
        """
        Let the agent play
        :param model: the network
        :param dev: the cude device
        :return:
        """
        # initialize environment
        totalreward = 0
        env.seed(300)
        dev = args.device
        episodes = 100
        model.eval()
        model = model.to(dev)
        for ep in range(episodes):
            obs = env.reset()
            # env.unwrapped.viewer.window.on_key_press = key_press

            done = False
            episode_reward = 0
            #self.prev_action = self.env.action_space.sample()
            cnt =  0
            while not done:
                cnt += 1
                env.render()
                inv = agent._extract_inv(obs).unsqueeze(0).to(args.device)
                obs = to_tensor(obs['pov'].copy()).unsqueeze(0).to(args.device)
                # obs = np.moveaxis(obs['pov'], 2, 0)  # move channels
                # obs = torch.from_numpy(obs.copy())
                # obs = obs.unsqueeze(0)
                #
                # if str(dev) == 'cpu':
                #     obs = obs.float()
                # obs = obs.to(dev)  # transfer to GPU
                # obs = obs.float()
                # forward
                with torch.set_grad_enabled(False):
                    output_tuple = model.act(obs, inv)

                action_dict = agent.insert_in_actiondict(output_tuple, env)
                # if np.random.random() < 0.92:
                #     action_dict = agent.convert_to_action(output_tuple, env)
                # else:
                #     action_dict = env.action_space.sample()

                obs, reward, done, _ = env.step(action_dict)
                if reward > 0:
                    print(reward, sep=' ', end='', flush=True)


                # if cnt == 5:
                #     show_image(env.render(mode = 'rgb_array'))
                #     cnt = 0
                episode_reward += reward

            print("\n____EPISODE____", ': \n', episode_reward, '\n_______________')
            totalreward += episode_reward

        print(totalreward/ episodes)
        #reward_list.append(episode_reward)
        #self.display_stats(reward_list)


    @staticmethod
    def display_stats(reward_list):
        plt.plot(range(len(reward_list)), reward_list)
        print(np.sum(reward_list)/ len(reward_list))
        plt.show()

    #@staticmethod



if __name__ == '__main__':
    pass
