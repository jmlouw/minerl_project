#!/usr/bin/env python
# PYTHON_ARGCOMPLETE_OK
"""
To run, use:
xvfb-run python3 main.py
"""
import sys

sys.path.insert(0, '/home/frana/Documents/minerl')
import minerl
import gym
import os
from pathlib import Path
import torch
from torch import nn
from itertools import count
import numpy as np
import matplotlib.pyplot as plt
import sys
from tqdm import tqdm
from ttictoc import TicToc
import time

# User imports...
from utils.utility_functions import print_b
from utils.gen_args import Arguments
from utils.tracking import StatTracker, HardwareTracker
# from utils.wrappers import EnvContainer
from vision.my_depth import MyDepthModel
from agents import run_clone
from agents.ppo_agent import PPOAgent
from agents.ppo_agent_1 import PPO1Agent
from agents.test_agent import TestAgent
from agents.a_setups.agent_disc_cam import Agent_Disc_Cam
from agents.a_setups.agent_cont_cam import Agent_Cont_Cam
from agents.a_setups.agent_branched import Agent_Branched
from agents.a_setups.agent_ import Agent_
from vision.dense_depth import ssim, DenseDepth, calc_accuracy


def main():
    my_args = Arguments()
    agents = {
        'discrete': Agent_Disc_Cam,
        'continuous': Agent_Cont_Cam,
        'branched': Agent_Branched,
        'agent_': Agent_
    }
    my_agent = agents[my_args.agent]()
    print('Agent Type: ', my_args.agent)
    print('Device:', my_args.device)

    stats = StatTracker()
    # model = MyDepthModel(args=args, loss_func=nn.L1Loss)
    try:
        # main(args=args, stats=stats, model= None)
        env = None
        if my_args.testing or my_args.operation == 'test' or my_args.operation == 'ppo' or my_args.operation == 'ppo1' or my_args.operation == 'preppo':
            print('Making Environment')
            env = gym.make(my_args.env_name)
            env = my_agent.wrap_environment(env, my_args)
        # env = None
        # testAgent = TestAgent(env, agent, args)
        # testAgent = TestAgent(env, agent, args)
        if my_args.operation == 'pretrain':
            run_clone.main(my_args, my_agent, env, data_name=my_args.env_name)
        elif my_args.operation == 'ppo':
            ppo_agent = PPOAgent(env=env, agent=my_agent, args=my_args)
            ppo_agent.agent_main(env=env, agent=my_agent, args=my_args)
        elif my_args.operation == 'ppo1':
            ppo1_agent = PPO1Agent(env=env, agent=my_agent, args=my_args)
            ppo1_agent.agent_main(env=env, agent=my_agent, args=my_args)
        elif my_args.operation == 'preppo':
            run_clone.main(my_args, my_agent, env, my_args.env_name)
            ppo_agent = PPOAgent(env=env, agent=my_agent, args=my_args)
            ppo_agent.agent_main(env=env, agent=my_agent, args=my_args)
        elif my_args.operation == 'preallenvs':
            for env_name in ['MineRLObtainIronPickaxeDense-v0', 'MineRLObtainDiamondDense-v0']:
                run_clone.main(my_args, my_agent, env, data_name=env_name)
        elif my_args.operation == 'test':
            env = gym.wrappers.Monitor(env, "./vid", video_callable=lambda episode_id: True, force=True)
            TestAgent(env, my_agent, my_args)
        # imitation_learning(args=args, stats=stats)
    except KeyboardInterrupt:
        pass


if __name__ == '__main__':
    main()
