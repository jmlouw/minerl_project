from unittest import TestCase
from utils.wrappers import discretise_camera
import numpy as np


class TestDiscretise_camera(TestCase):
    def test_discretise_camera(self):
        output = discretise_camera(np.array([[-11, 11],
                                             [-10, 10],
                                             [-7, 7],
                                             [-4, 4],
                                             [-3, 3],  # 0  1  2   3
                                             [-2, 2],
                                             [-1, 1],
                                             [0, 0]]), np.array([0, 2, 4, 10]))
        print(output)

        output = discretise_camera(np.array([[-11, 11],
                                             [-10, 10],
                                             [-7, 7],
                                             [-4, 4],
                                             [-3, 3],  # 0  1  2   3
                                             [-2, 2],
                                             [-1, 1],
                                             [0, 0]]), np.array([0, 8]))
        print(output)

# INDEX:   0,  1,  2, 3, 4, 5, 6
# VALUE: -10, -4, -2, 0, 2, 4, 10
