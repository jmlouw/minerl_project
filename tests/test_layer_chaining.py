import unittest
import torch
from torch import nn

from utils.utility_functions import CNNBlock, LinearBlock, TransposeCNNBlock
from utils.gen_args import Arguments
from vision.my_depth import MyDepthModel


class TestChains(unittest.TestCase):
    def test_layer_chain(self):
        width = 2 ** 6
        height = 2 ** 7
        batch_size = 4
        conv = CNNBlock(
            in_channels=1,
            out_channels=2,
            kernel_size=2,
            stride=2,
            in_width=width,
            in_height=height
        )
        nxt_conv = CNNBlock(
            out_channels=4,
            kernel_size=2,
            stride=2,
            prev_layer=conv
        )

        linear1 = LinearBlock(prev_layer=nxt_conv, out_features=64)
        layerlist = [conv, nxt_conv, linear1]
        for i in range(5):
            layerlist.append(
                LinearBlock(
                    prev_layer=layerlist[-1],  # Previous layer
                    out_features=2 ** (i + 7)
                )
            )
        layerlist.append(
            TransposeCNNBlock(
                in_channels=4,
                out_channels=2,
                kernel_size=2,
                stride=2,
                in_height=int(nxt_conv.out_height),
                in_width=int(nxt_conv.out_width)
            )
        )
        layerlist.append(
            TransposeCNNBlock(
                out_channels=1,
                kernel_size=2,
                stride=2,
                prev_layer=layerlist[-1]
            )
        )

        sample_data = torch.randn(size=(batch_size, 1, height, width))
        before_shape = sample_data.shape

        seq_layers = nn.Sequential(*layerlist)
        sample_data = seq_layers(sample_data)
        self.assertEqual(before_shape, sample_data.shape)

    def test_depth_model(self):
        args = Arguments()

        width = 64
        height = 64
        batch_size = 8

        model = MyDepthModel(args=args)

        sample_data = torch.randn(size=(batch_size, 3, height, width)).to(args.device)

        sample_data = model(sample_data)
        self.assertEqual(sample_data.shape, (batch_size, 1, height, width))


if __name__ == '__main__':
    unittest.main()
