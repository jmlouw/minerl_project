from unittest import TestCase
import unittest
import gym
import minerl
import torch
import torch_testing as tt
from collections import OrderedDict
from agents.ppo_agent import PPOAgent
import numpy as np
import numpy.testing


class Testbc(TestCase):

    @classmethod
    def setUpClass(self) -> None:
        self.test_agent = PPOAgent()
        self.env = gym.make("MineRLTreechop-v0")

    def test_to_action_dict(self):
        self.env.reset()
        for i in range (7):
            action = self.test_agent.to_action_dict(i, self.env)
            print(action)
            self.env.step(action)



if __name__ == '__main__':
    unittest.main()