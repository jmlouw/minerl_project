from unittest import TestCase
import torch
import torch_testing as tt
from agents.run_clone import read_dataset, generate_labels
import collections
import numpy as np


class TestRunClone(TestCase):

    @classmethod
    def setUpClass(self) -> None:
        training_data = read_dataset('minerl_data_set', 'MineRLObtainDiamondDense-v0', num_workers=8)
        train_iter = training_data.sarsd_iter(num_epochs=1, max_sequence_len=4)
        current_state, self.action, reward, next_state, done = next(train_iter)
        print(self.action)
        training_data.processing_pool.close()
        training_data.processing_pool.terminate()
        # print(action)

    def test_read_dataset(self):
        my_data = read_dataset('minerl_data_set', 'MineRLTreechop-v0', num_workers=8)

    def test_generate_labels(self):
        # f -> 6
        action1 = collections.OrderedDict(
            [('attack', np.array([0, 0, 1, 0])), ('back', np.array([0, 0, 0, 0])), ('camera', np.array([[-20., 21.],
                                                                                                        [-5., 4.],
                                                                                                        [0., 0.],
                                                                                                        [0., 0.]],
                                                                                                       dtype=np.float32)),
             ('craft', np.array([0, 0, 0, 0])), ('equip', np.array([0, 0, 0, 0])), ('forward', np.array([1, 1, 1, 1])), ('jump', np.array([0, 1, 1, 0])),
             ('left', np.array([0, 0, 0, 0])), ('nearbyCraft', np.array([0, 0, 0, 0])), ('nearbySmelt', np.array([0, 0, 0, 0])),
             ('place', np.array([0, 0, 0, 1])), ('right', np.array([0, 0, 0, 0])), ('sneak', np.array([0, 0, 0, 0])), ('sprint', np.array([0, 0, 0, 0]))])

        label_dict = generate_labels(action1)
        print(label_dict['btn'])
        tt.assert_equal(label_dict['btn'], torch.tensor([6, 3, 1, 8]))
        tt.assert_equal(label_dict['pitch'], torch.tensor([-10., -5., 0, 0]))
        tt.assert_equal(label_dict['yaw'], torch.tensor([10., 4., 0, 0]))



        # # af -> 2
        # action2 = collections.OrderedDict(
        #     [('attack', np.array([1])), ('back', np.array([0])), ('camera', np.array([[5., -6.]], dtype=np.float32)),
        #      ('craft', np.array([0])), ('equip', np.array([0])), ('forward', np.array([1])), ('jump', np.array([0])),
        #      ('left', np.array([0])), ('nearbyCraft', np.array([0])), ('nearbySmelt', np.array([0])),
        #      ('place', np.array([0])), ('right', np.array([0])), ('sneak', np.array([0])), ('sprint', np.array([0]))])
        # label_dict = generate_labels(action2)
        # tt.assert_equal(label_dict['btn'], torch.tensor([2]))
        # tt.assert_equal(label_dict['pitch'], torch.tensor([5.]))
        # tt.assert_equal(label_dict['yaw'], torch.tensor([-6.]))
        #
        # # afj -> 1
        # action3 = collections.OrderedDict(
        #     [('attack', np.array([1])), ('back', np.array([0])), ('camera', np.array([[9., 11.]], dtype=np.float32)),
        #      ('craft', np.array([0])), ('equip', np.array([0])), ('forward', np.array([1])), ('jump', np.array([1])),
        #      ('left', np.array([0])), ('nearbyCraft', np.array([0])), ('nearbySmelt', np.array([0])),
        #      ('place', np.array([0])), ('right', np.array([0])), ('sneak', np.array([0])), ('sprint', np.array([0]))])
        # label_dict = generate_labels(action3)
        # tt.assert_equal(label_dict['btn'], torch.tensor([1]))
        # tt.assert_equal(label_dict['pitch'], torch.tensor([9.]))
        # tt.assert_equal(label_dict['yaw'], torch.tensor([10.]))
