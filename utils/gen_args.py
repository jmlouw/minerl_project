#!/usr/bin/env python
# PYTHON_ARGCOMPLETE_OK
import argparse
import argcomplete
import torch
from ttictoc import TicToc
import os
import numpy as np

# from .tracking import HardwareTracker


dir_path = os.path.dirname(os.path.realpath(__file__))


class Arguments:
    def __init__(self):
        self.parser = argparse.ArgumentParser()
        #  Add arguments here
        self.parser.add_argument(
            '--agent', type=str, default='agent_',
            choices=[
                'discrete',
                'continuous',
                'branched',
                'agent_'
            ],
            help='Choose camera discrete or continuous.'
        )

        self.add_int(short='b', name='batch-size', default=32, description='Size of batches')
        self.add_int(short='me', name='max-episodes', default=1000, description='Size of batches')
        self.add_float(short='--lr', name='learning-rate', default=0.00001, description='Initial learning rate')
        self.add_float(short='ec', name='eps-clip', default=0.2, description='Clip parameter for PPO')
        self.add_float(short='g', name='gamma', default=0.99, description='Discount factor')
        self.add_int(short='ke', name='K-epochs', default=4, description='Number epochs')
        self.add_false(short='rstr', name='restore', description='Restore from previous model')
        self.add_true(name='testing', description='Test the agent during pretraining')
        self.add_int(short='e', name='epochs', default=1000, description='Number of epochs')
        self.add_int(short='mt', name='max_timesteps', default=1000, description='Max steps')
        self.add_str(short='op', name='operation', default='preppo', description='Operation to execude')
        self.add_str(short='en', name='env_name', default='MineRLTreechop-v0', description='Environment to generate:'
                                                                                           'MineRLTreechop-v0, '
                                                                                           'MineRLObtainIronPickaxe-v0, '
                                                                                           'MineRLObtainIronPickaxeDense-v0 , '
                                                                                           'MineRLObtainDiamond-v0, '
                                                                                           'MineRLObtainDiamondDense-v0')
        self.add_str(short='mp', name='model_path', default='default', description='Path where model is saved.')

        #  End arguments add
        argcomplete.autocomplete(self.parser)
        self.args, *_ = self.parser.parse_known_args()

        #  Manually add arguments here
        self.args.episode = 0
        self.args.bins = np.array([6])
        self.args.device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
        self.args.pretrain_steps = 80000

        if self.args.model_path == 'default':
            if self.args.operation == 'preallenvs':
                self.args.model_path = self.args.agent + '_' + 'preallenvs' + '.pt'
            else:
                self.args.model_path = self.args.agent+ '_' + self.args.env_name +'.pt'

#  End manual argument add
        self.__dict__.update(vars(self.args))
        #  List of modules to import needed for data types. Prepended by 'import ', so use that to
        #  construct more specific imports.
        import_list = ['argparse', 'torch']
        update_stub(self, imports=import_list, this_file=os.path.basename(__file__))

    def add_int(self, name: str, default: int, description: str, short: str = None, meta: str = 'N') -> None:
        name, short = add_dashes(name=name, short=short)
        if short is not None:
            self.parser.add_argument(short, name, type=int, default=default, metavar=meta,
                                     help=f'{description} (default: {default})')
        else:
            self.parser.add_argument(name, type=int, default=default, metavar=meta,
                                     help=f'{description} (default: {default})')

    def add_float(self, name: str, default: float, description: str, short: str = None, meta: str = 'N') -> None:
        name, short = add_dashes(name=name, short=short)
        if short is not None:
            self.parser.add_argument(short, name, type=float, default=default, metavar=meta,
                                     help=f'{description} (default: {default})')
        else:
            self.parser.add_argument(name, type=float, default=default, metavar=meta,
                                     help=f'{description} (default: {default})')

    def add_str(self, name: str, default: str, description: str, short: str = None, meta: str = 'str') -> None:
        name, short = add_dashes(name=name, short=short)
        if short is not None:
            self.parser.add_argument(short, name, type=str, default=default, metavar=meta,
                                     help=f'{description} (default: {default})')
        else:
            self.parser.add_argument(name, type=str, default=default, metavar=meta,
                                     help=f'{description} (default: {default})')

    def add_true(self, name: str, description: str, short: str = None) -> None:
        name, short = add_dashes(name=name, short=short)
        if short is not None:
            self.parser.add_argument(short, name, action='store_true', default=False,
                                     help=f'{description} (default: False)')
        else:
            self.parser.add_argument(name, action='store_true', default=False,
                                     help=f'{description} (default: False)')

    def add_false(self, name: str, description: str, short: str = None) -> None:
        name, short = add_dashes(name=name, short=short)
        if short is not None:
            self.parser.add_argument(short, name, action='store_false', default=True,
                                     help=f'{description} (default: True)')
        else:
            self.parser.add_argument(name, action='store_false', default=True,
                                     help=f'{description} (default: True)')

    def __getattr__(self, item) -> object:
        assert item in vars(self).keys(), f"Key '{item}' not found in args. Check if it is still in argparse."
        return vars(self.args)[item]


def update_stub(cls: object, this_file: str, imports: list = None) -> None:
    """
    Creates stub based on dictionary to allow for auto-completion after dynamically adding variables to args. Needs to
    be executed once before providing autocomplete functionality.
    :param cls: Instance of class. If called from inside class, simply send self
    :param this_file:
    :param imports: List of libraries needed for data types. For example to save numpy array type, we need to import
                    numpy.
    :return: None
    """
    dictionary = vars(cls).copy()
    cls_name = cls.__class__.__name__
    if imports is not None:
        assert isinstance(imports[0], str)
    dictionary = vals_to_type(dictionary.copy())
    file = ''.join([dir_path, '/', this_file, 'i'])
    with open(file=file, mode='w') as stub:
        stub_cont = []
        for i in range(len(imports)):
            item = imports[i]
            if item == 'numpy':
                item = 'numpy as np'
            stub_cont.append(f'import {item}')
        stub_cont.append(f'class {cls_name}:')
        for key, value in dictionary.items():
            stub_cont.append(f"    {key}: {value}")
        stub_cont.append('')
        stub_cont = '\n'.join(stub_cont)
        stub.write(stub_cont)


def vals_to_type(dictionary: dict) -> dict:
    for key in dictionary.keys():
        arg_type = str(type(dictionary[key])).split("'")[1]  # Get type as string
        dictionary[key] = arg_type
    return dictionary


def add_dashes(name: str = None, short: str = None):
    if name is not None and not name.startswith('--'):
        name = ''.join(['--', name])
    if short is not None and not short.startswith('-'):
        short = ''.join(['-', short])
    return name, short


if __name__ == '__main__':
    args = Arguments()
    print(args.device)
    # card id 0 hardcoded here, there is also a call to get all available card ids, so we could iterate
