from dataclasses import dataclass
from ttictoc import TicToc
#import torch
import numpy as np
import datetime
from matplotlib import pyplot as plt
from statistics import mean
import psutil
import sys
from collections import deque
from .utility_functions import print_b

try:
    import nvidia_smi
except Exception as e:
    print('Module nvidia-smi not installed. Install to view GPU usage.')


@dataclass
class StatTracker:
    """
    Class to track stats listed below. Also some functions extend the use cases.
    """
    episode: int = 0
    step: int = 0
    total_steps: int = 0
    loss: object = 0.0
    epsilon: float = 0.0
    rewards: list = None
    timer: TicToc = TicToc()

    def __post_init__(self):
        """
        Gets called after the default init of a dataclass
        """
        self.timer.set_print_toc(False)
        self.rewards = []

    def init_tracker(self, **kwargs):
        """
        Initializes instance to values given in kwargs. Starts timer and accounts for continued runs if given a
        'duration' key.
        :param kwargs:
        :return: None
        """
        for k, v in kwargs.items():
            if k in self.__dict__.keys():
                #                 self.__dict__.update((k, v))
                setattr(self, k, v)
            elif k is not 'duration':
                print(f'There is no {k} key in the StatTracker class.')

        self.start_timer()
        if 'duration' in kwargs:
            self.timer.tstart -= kwargs['duration']

    def start_timer(self):
        self.timer.tic()

    def get_time_str(self):
        """
        Get time in a readable format. (00h 00m 00s)
        """
        duration = self.timer.toc()
        return '{:02d}h {:02d}m {:02d}s'.format(
            int(duration / 3600), int((duration % 3600) / 60), int(duration % 60)
        )

    def append_reward(self, reward):
        self.rewards.append(reward)

    def update_total_steps(self):
        self.total_steps += self.step + 1

    def set_loss(self, loss):
        pass
        # assert not isinstance(loss, tuple)
        # if isinstance(loss, torch.Tensor):
        #     loss = loss.item()
        # self.loss = loss

    def finish_episode(self, verbose, reward, loss, agent_eps=None):
        """
        Update parameters and print current progress
        :param reward: Final reward to append
        :param loss: Loss to set for printing
        :param verbose: Print current progress
        :param agent_eps: Use epsilon from agent
        :return:
        """
        # self.get_epsilon(agent_eps=agent_eps)
        self.append_reward(reward)
        self.update_total_steps()
        self.set_loss(loss)
        if verbose:
            self.print_progress()

    def print_progress(self, tqdm_print=True):
        duration_str = ''.join([self.get_time_str(), ' reward: {:.3f}'.format(
            self.rewards[-1])]
        )
        str2 = 'global_step: {}, eps: {:.3f}, '.format(
            self.total_steps, self.epsilon
        )
        str3 = 'loss: {:.4f}, avg score: {:.3f}, step: {}'.format(
            self.loss, self.av_reward, self.step + 1
        )

        if tqdm_print:
            print_b('\t'.join([duration_str, str2, str3]))
        else:
            print('\t'.join([duration_str, str2, str3]))

    def plot_results(self):
        x_axis = np.arange(0, self.episode+1)
        plt.plot(x_axis, self.rewards, 'r')
        time_label = datetime.datetime.now().strftime("%Y-%m-%d %H:%M")
        plt.savefig("results/tabular_qlearning/" + time_label)

    @property
    def av_reward(self):
        if len(self.rewards) < 100:
            return mean(self.rewards)
        else:
            return mean(self.rewards[-100:])

    @property
    def curr_total_steps(self):
        return self.total_steps + self.step


class HardwareTracker:
    def __init__(self):
        self.nvidia_smi_enabled = False
        maxlen = 20
        self.ram = deque(maxlen=maxlen)
        self.cpu = deque(maxlen=maxlen)
        self.gpu = None
        self.gpu_mem = None

        if 'nvidia_smi' in sys.modules:  # Check GPU usage
            self.nvidia_smi_enabled = True
            nvidia_smi.nvmlInit()
            self.handle = nvidia_smi.nvmlDeviceGetHandleByIndex(0)
            self.res = nvidia_smi.nvmlDeviceGetUtilizationRates(self.handle)
            self.gpu = deque(maxlen=maxlen)
            self.gpu_mem = deque(maxlen=maxlen)

    def __str__(self):
        self.ram.append(int(round(psutil.virtual_memory().percent)))
        self.cpu.append(int(round(psutil.cpu_percent())))
        ram_usage = f'RAM:{mean(self.ram):2.0f}%'
        cpu_usage = f'CPU:{mean(self.cpu):2.0f}%'
        gpu_usage = ''
        if self.nvidia_smi_enabled:
            self.gpu.append(int(round(self.res.gpu)))
            self.gpu_mem.append(int(round(self.res.memory)))
            gpu_usage = f'GPU:{mean(self.gpu)}%, GPU_M:{mean(self.gpu_mem)}%'
        return ', '.join([cpu_usage, ram_usage, gpu_usage])
