"""
Environment wrappers and helpful functions that do not fit
nicely into any other file.
"""

import os
import torch

def checkpoint_save(obj, path):
    """
    Save a model to a file, making sure that the file will
    never be partly written.
    This prevents the model from getting corrupted in the
    event that the process dies or the machine crashes.
    """
    torch.save(obj, path + '.tmp')
    os.rename(path + '.tmp', path)

def checkpoint_load(actor, optimizer_actor, critic, optimizer_critic, pretrain: bool, path, device):
    if os.path.exists(path):
        print("Loading model...")
        loadpoint = torch.load(path, map_location= device)
        actor.load_state_dict(loadpoint['actor'])
        critic.load_state_dict(loadpoint['critic'])

        if pretrain:
            optimizer_actor.load_state_dict(loadpoint['optimizer_actor'])
            optimizer_critic.load_state_dict(loadpoint['optimizer_critic'])
            for state in optimizer_actor.state.values():
                for k, v in state.items():
                    if isinstance(v, torch.Tensor):
                        state[k] = v.to(device)
            for state in optimizer_critic.state.values():
                for k, v in state.items():
                    if isinstance(v, torch.Tensor):
                        state[k] = v.to(device)

            return loadpoint['step']


def checkpoint_load_actor(actor, path):
    if os.path.exists(path):
        print("Loading model...")
        loadpoint = torch.load(path)
        actor.load_state_dict(loadpoint['actor'])
