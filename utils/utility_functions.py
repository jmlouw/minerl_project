from tqdm import tqdm
import numpy as np
import torch
from torch import nn
from dataclasses import dataclass
from torchvision.transforms import ToTensor


def print_b(text):
    """
    Print function for when progressbar is running
    :param text: Text to print
    :return: None
    """
    tqdm.write(str(text))


def get_envs_list():
    return None


class Flatten(nn.Module):
    def __init__(self):
        super(Flatten, self).__init__()

    def forward(self, x: torch.Tensor):
        batch_size = x.shape[0]
        return x.view(batch_size, -1)


def to_tensor(obs, transform=ToTensor()):
    if isinstance(obs, list):
        obs = np.asarray(obs)
    if isinstance(obs, np.ndarray):
        assert 2 < obs.ndim < 5, 'Observation ndims is not within the range [3, 4]'
        if obs.ndim == 3:  # Single image observation
            return transform(obs)
        elif obs.ndim == 4:  # Batch of images, need to stack tensor
            return torch.stack(
                [transform(obs_batch) for obs_batch in obs]
            )
    return obs  # Was  already tensor


class OneDto3D(nn.Module):
    def __init__(self, height, width):
        super(OneDto3D, self).__init__()
        self.height = height
        self.width = width

    def forward(self, x):
        x = x.view(x.shape[0], -1, self.height, self.width)
        return x


@dataclass
class HWDataConv2d:
    """Not really necessary, but helps with Type Hinting, since you cannot set the class its self as a type hint."""
    in_width: int = None
    in_height: int = None
    out_channels: int = None
    out_width: int = None
    out_height: int = None


class ChainConv2d(nn.Conv2d, HWDataConv2d):
    def __init__(self,
                 out_channels,
                 kernel_size,
                 in_channels=0,
                 stride=1,
                 padding=0,
                 dilation=1,
                 groups=1,
                 bias=True,
                 padding_mode='zeros',
                 in_width: int = 0,
                 in_height: int = 0,
                 prev_layer: HWDataConv2d = None):
        """
        Allows 2D convolutional layers to be chained while automatically keeping track of width & height values
        :param out_channels: Conv2D parameter
        :param kernel_size: Conv2D parameter
        :param in_channels: Conv2D parameter, if not integer bigger than zero,
        assumes previous layer is provided (default: 0)
        :param stride: Conv2D parameter
        :param padding: Conv2D parameter
        :param dilation: Conv2D parameter
        :param groups: Conv2D parameter
        :param bias: Conv2D parameter
        :param padding_mode: Conv2D parameter
        :param in_width: width of input tensor, if 0: assume previous layer is given (img.shape[-1])
        :param in_height: height of input tensor, if 0: assume previous layer is given (img.shape[-2])
        :param prev_layer: previous layer. If given obtains in_width,
        in_height and in_channels from layer. (default: None)
        """
        if (in_channels == 0 and (in_height == 0 or in_width == 0)) and prev_layer is None:
            raise ValueError(f'Need either in_channel & width & height values or the previous CNN layer.')
        if prev_layer is not None:
            in_width = prev_layer.out_width
            in_height = prev_layer.out_height
            in_channels = prev_layer.out_channels
        else:
            self.out_channels = in_channels
            self.in_height = in_height
            self.in_width = in_width

        super(ChainConv2d, self).__init__(in_channels, out_channels, kernel_size, stride, padding, dilation, groups,
                                          bias, padding_mode)
        self.out_height = (in_height+2*self.padding[0]-self.dilation[0]*(self.kernel_size[0]-1)-1)/self.stride[0]+1
        self.out_width = (in_width+2*self.padding[1]-self.dilation[1]*(self.kernel_size[1]-1)-1)/self.stride[1]+1
        assert self.out_height == int(self.out_height), f'Out height value is not integer. Got {self.out_height}.'
        assert self.out_width == int(self.out_width), f'Out width value is not integer. Got {self.out_width}.'

    def get_out_sizes(self):
        return int(self.out_height), int(self.out_width)


class ChainDeConv2d(nn.ConvTranspose2d, HWDataConv2d):
    def __init__(self,
                 out_channels,
                 kernel_size,
                 in_channels=0,
                 stride=1,
                 padding=0,
                 output_padding=0,
                 dilation=1,
                 groups=1,
                 bias=True,
                 padding_mode='zeros',
                 in_width: int = 0,
                 in_height: int = 0,
                 prev_layer: HWDataConv2d = None):
        """
        Allows chaining of transpose 2d convolution layers  while automatically keeping track of width & height values
        :param out_channels: Conv2d parameter
        :param kernel_size: Conv2d parameter
        :param in_channels: Conv2d parameter, if not integer bigger than zero,
        assumes previous layer is provided (default: 0)
        :param stride:  Conv2d parameter
        :param padding:  Conv2d parameter
        :param output_padding:  Conv2d parameter
        :param dilation:  Conv2d parameter
        :param groups:  Conv2d parameter
        :param bias:  Conv2d parameter
        :param padding_mode:  Conv2d parameter
        :param in_width: width of input tensor, if 0: assume previous layer is given (img.shape[-1])
        :param in_height: height of input tensor, if 0: assume previous layer is given (img.shape[-2])
        :param prev_layer: previous layer. If given obtains in_width,
        in_height and in_channels from layer. (default: None)
        """
        if (in_channels == 0 and (in_height == 0 or in_width == 0)) and prev_layer is None:
            raise ValueError(f'Need either in_channel & width & height values or the previous CNN layer.')
        if prev_layer is not None:
            in_width = prev_layer.out_width
            in_height = prev_layer.out_height
            in_channels = prev_layer.out_channels
        else:
            self.out_channels = in_channels
            self.in_height = in_height
            self.in_width = in_width

        super(ChainDeConv2d, self).__init__(in_channels=in_channels, out_channels=out_channels, kernel_size=kernel_size,
                                            stride=stride, padding=padding, output_padding=output_padding,
                                            groups=groups, bias=bias, dilation=dilation, padding_mode=padding_mode)
        self.out_height = (in_height-1)*self.stride[0] - 2*self.padding[0] + self.dilation[0]*(self.kernel_size[0]-1) +\
                          self.output_padding[0] + 1
        self.out_width = (in_width-1)*self.stride[1] - 2*self.padding[1] + self.dilation[1]*(self.kernel_size[1]-1) +\
                          self.output_padding[1] + 1
        assert self.out_height == int(self.out_height), f'Out height value is not integer. Got {self.out_height}.'
        assert self.out_width == int(self.out_width), f'Out width value is not integer. Got {self.out_width}.'

    def get_out_sizes(self):
        return int(self.out_height), int(self.out_width)


class ChainLinear(nn.Linear):
    def __init__(self,
                 out_features: int,
                 in_features: int = 0,
                 bias: bool = True,
                 prev_layer: nn.Module = None):
        """
        Allows chaining linear layers by providing preceding layer. Also applies flattening if required.
        :param out_features: Linear parameter
        :param in_features: Linear parameter, if not integer bigger than zero,
        assumes previous layer is provided (default: 0)
        :param bias: Linear parameter
        :param prev_layer:  previous layer. If given obtains in_features from layer. (default: None)
        """
        if in_features == 0 and prev_layer is None:
            raise ValueError(f'Need either in_features value or the previous layer.')
        if prev_layer is not None and isinstance(prev_layer, nn.Linear):
            in_features = prev_layer.out_features
        elif in_features != 0:
            self.in_features = in_features
        elif prev_layer is not None and isinstance(prev_layer, ChainConv2d):
            in_features = int(prev_layer.out_height * prev_layer.out_width * prev_layer.out_channels)

        super(ChainLinear, self).__init__(in_features, out_features, bias)


class CNNBlock(nn.Module):
    def __init__(self,
                 out_channels,
                 kernel_size,
                 in_channels=0,
                 stride=1,
                 padding=0,
                 dilation=1,
                 groups=1,
                 bias=True,
                 padding_mode='zeros',
                 in_width: int = 0,
                 in_height: int = 0,
                 prev_layer: ChainConv2d = None,
                 use_bn: bool = True,
                 activation: nn.Module = nn.ReLU):
        super(CNNBlock, self).__init__()
        if isinstance(prev_layer, TransposeCNNBlock) or \
                isinstance(prev_layer, CNNBlock) or \
                isinstance(prev_layer, LinearBlock):
            prev_layer = prev_layer.layer
        self.layer = ChainConv2d(out_channels, kernel_size, in_channels, stride, padding, dilation, groups, bias,
                                 padding_mode, in_width, in_height, prev_layer)
        self.use_bn = use_bn
        self.bn = None
        if self.use_bn:
            self.bn = nn.BatchNorm2d(self.layer.out_channels)
        self.activation = activation()

    def forward(self, x):
        x = self.layer(x)
        if self.use_bn:
            x = self.bn(x)
        return self.activation(x)

    def __str__(self):
        return f"""{self.__class__.__name__}(
        CNN: in_channels={self.in_channels}, out_channels={self.out_channels}, kernel_size={self.kernel_size}, \
stride={self.stride}
        BatchNorm2d: {str(self.bn)}
        Activation: {str(self.activation)}\n)
        """

    @property
    def out_height(self):
        return self.layer.out_height

    @property
    def out_width(self):
        return self.layer.out_width


class TransposeCNNBlock(nn.Module):
    def __init__(self,
                 out_channels,
                 kernel_size,
                 in_channels=0,
                 stride=1,
                 padding=0,
                 output_padding=0,
                 dilation=1,
                 groups=1,
                 bias=True,
                 padding_mode='zeros',
                 in_width: int = 0,
                 in_height: int = 0,
                 prev_layer: ChainDeConv2d = None,
                 use_bn: bool = True,
                 activation: nn.Module = nn.ReLU):
        super(TransposeCNNBlock, self).__init__()
        if isinstance(prev_layer, TransposeCNNBlock) or \
                isinstance(prev_layer, CNNBlock) or \
                isinstance(prev_layer, LinearBlock):
            prev_layer = prev_layer.layer
        self.layer = ChainDeConv2d(out_channels, kernel_size, in_channels, stride, padding, output_padding,
                                   dilation, groups, bias, padding_mode, in_width, in_height, prev_layer)
        self.use_bn = use_bn
        self.bn = None
        self.in_height = in_height
        self.in_width = in_width
        if self.use_bn:
            self.bn = nn.BatchNorm2d(self.layer.out_channels)
        self.activation = activation()

    def forward(self, x, output_size=None):
        if self.in_width != 0 and self.in_height != 0:
            x = x.view(x.shape[0], -1, self.in_height, self.in_width)
        x = self.layer(x, output_size)
        if self.use_bn:
            x = self.bn(x)
        return self.activation(x)

    def __str__(self):
        return f"""{self.__class__.__name__}(
        CNN: in_channels={self.in_channels}, out_channels={self.out_channels}, kernel_size={self.kernel_size}, \
stride={self.stride}
        BatchNorm2d: {str(self.bn)}
        Activation: {str(self.activation)}\n)
        """

    @property
    def out_height(self):
        return self.layer.out_height

    @property
    def out_width(self):
        return self.layer.out_width


class LinearBlock(nn.Module):
    def __init__(self,
                 out_features: int,
                 in_features: int = 0,
                 bias: bool = True,
                 prev_layer: nn.Linear = None,
                 use_bn: bool = True,
                 activation: nn.Module = nn.ReLU):
        super(LinearBlock, self).__init__()
        if isinstance(prev_layer, TransposeCNNBlock) or \
                isinstance(prev_layer, CNNBlock) or \
                isinstance(prev_layer, LinearBlock):
            prev_layer = prev_layer.layer
        self.layer = ChainLinear(out_features, in_features, bias, prev_layer)
        self.prev_conv = None if not isinstance(prev_layer, ChainConv2d) else prev_layer
        self.use_bn = use_bn
        self.bn = None
        if self.use_bn:
            self.bn = nn.BatchNorm1d(self.layer.out_features)
        self.activation = activation()

    def forward(self, x):
        if self.prev_conv is not None:
            x = x.view(x.shape[0], -1)
        x = self.layer(x)
        if self.use_bn:
            x = self.bn(x)
        return self.activation(x)

    def __str__(self):
        return f"""{self.__class__.__name__}(
        Linear: in_features={self.in_features}, out_features={self.out_features}, bias={self.bias is not None}
        BatchNorm1d: {str(self.bn)}
        Activation: {str(self.activation)}\n)
        """


if __name__ == '__main__':
    pass
