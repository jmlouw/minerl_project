import gym
import numpy as np
import copy
import minerl
from minerl.env.spaces import Discrete, Dict, Box
from itertools import repeat
from collections import OrderedDict

# from .tracking import StatTracker
# from .gen_args import Arguments


# class EnvContainer:
#     def __init__(self, args: Arguments, stats: StatTracker,  env_name='MineRLNavigateDense-v0', render_wrap=True):
#         self.env = gym.make(env_name)
#         self.args = args
#         self.stats = stats
#         self.action_space = self.env.action_space
#         self.nr_actions = len(self.action_space.spaces.items())
#         if render_wrap:
#             self.env = RenderObservations(self.env)
#
#     def reset(self):
#         return self.env.reset()
#
#     def step(self, action: int):
#         return self.env.step(action)
#
#     def sample_action(self):
#         return self.action_space.sample()
#
#     def render(self, mode='human'):
#         self.env.render(mode)
#
#     def seed(self, seed: int):
#         self.env.seed(seed)
#
#     @property
#     def spec(self):
#         return self.env.spec
from utils.gen_args import Arguments


class ActionConverter:
    """
    branch for none/attack
    branch for none/forward
    branch for none/jump
    branch for none/sprint/left/right
    branch for camera0
    branch for camera1
    branch for none/*place/*equip/*craft/*nearbyCraft/*nearbySmelt
    """
    CRAFTING_ACTIONS = ['place', 'equip', 'craft', 'nearbyCraft', 'nearbySmelt']
    BINARY_KEYS = ['forward', 'back', 'left', 'right', 'jump', 'sneak', 'sprint', 'attack']

    PLACE_OPTIONS = ['none', 'dirt', 'stone', 'cobblestone', 'crafting_table', 'furnace', 'torch']
    EQUIP_OPTIONS = ['none', 'air', 'wooden_axe', 'wooden_pickaxe', 'stone_axe',
                     'stone_pickaxe', 'iron_axe', 'iron_pickaxe']
    CRAFT_OPTIONS = ['none', 'torch', 'stick', 'planks', 'crafting_table']
    NEARBY_CRAFT_OPTIONS = ['none', 'wooden_axe', 'wooden_pickaxe', 'stone_axe',
                            'stone_pickaxe', 'iron_axe', 'iron_pickaxe', 'furnace']
    NEARBY_SMELT_OPTIONS = ['none', 'iron_ingot', 'coal']

    def __init__(self, actions):
        ActionConverter.generate_labels_combined(actions)
        return None

    @classmethod
    def craft_labels(cls, actions: dict):
        offset = 0
        nearby_smelt = actions.pop('nearbySmelt', None)
        nearby_craft = actions.pop('nearbyCraft', None)
        craft = actions.pop('craft', None)
        equip = actions.pop('equip', None)
        place = actions.pop('place', None)

        items = place  # lowest priority
        offset += len(cls.PLACE_OPTIONS) - 1
        items[equip != 0] = offset + equip[equip != 0]
        offset += len(cls.EQUIP_OPTIONS) - 1
        items[craft != 0] = offset + craft[craft != 0]
        offset += len(cls.CRAFT_OPTIONS) - 1
        items[nearby_craft != 0] = offset + nearby_craft[nearby_craft != 0]
        offset += len(cls.NEARBY_CRAFT_OPTIONS) - 1
        items[nearby_smelt != 0] = offset + nearby_smelt[nearby_smelt != 0]
        actions['item'] = items

    @classmethod
    def generate_labels_combined(cls, actions: dict, args: Arguments, discrete_cam=True):
        """
        :param actions: dict of lists containing action at time step t
        :return: actions
        """
        exclude_actions(actions, ['back', 'left', 'right', 'sneak', 'sprint'])

        # obtain_env =
        if discrete_cam:
            camera = discretise_camera(actions.pop('camera', None), args.bins)
        else:
            camera = np.clip(actions.pop('camera', None), -10, 10)
        # labels pitch
        actions['pitch'] = camera[:, 0]
        # labels yaw
        actions['yaw'] = camera[:, 1]
        # labels button
        # action - {'attack': np.array[0, 0, 1, 0], 'forward': np.array[0, 0, 1, 0]}
        attack = actions.pop('attack', None)  # len = (seq_len,)  np.ndarray
        not_attack = np.logical_not(attack)

        forward = actions.pop('forward', None)
        not_forward = np.logical_not(forward)

        jump = actions.pop('jump', None)
        not_jump = np.logical_not(jump)

        def log_and2(x, y):
            return np.logical_and(x, y).astype(np.long)

        def log_and3(x, y, z):
            return log_and2(log_and2(x, y), z)

        # act = [1, 1, 0, 0 , 0, 0, 0]
        act = np.zeros((len(forward), 8))

        act[:, 0] = 1  # None -> 0
        act[:, 1] = log_and3(attack, forward, jump)  # afj -> 1
        act[:, 2] = log_and3(attack, forward, not_jump)  # af -> 2
        act[:, 3] = log_and3(not_attack, forward, jump)  # fj -> 3
        act[:, 4] = log_and3(attack, not_forward, jump)  # aj -> 4
        act[:, 5] = log_and3(attack, not_forward, not_jump)  # a -> 5
        act[:, 6] = log_and3(not_attack, forward, not_jump)  # f -> 6
        act[:, 7] = log_and3(not_attack, not_forward, jump)  # j -> 7
        actions['act'] = (act.shape[-1] - np.argmax(np.flip(act, axis=1), axis=1) - 1)

        # Priority: nearbySmelt, nearbyCraft, craft, equip, place
        if 'craft' in actions:
            cls.craft_labels(actions)

    @classmethod
    def generate_labels_branched(cls, actions: dict, args: Arguments, discrete_cam=True):
        """
        :param actions: dict of lists containing action at time step t
        :return: actions
        """
        exclude_actions(actions, ['back', 'left', 'right', 'sneak', 'sprint'])

        # obtain_env =
        if discrete_cam:
            camera = discretise_camera(actions.pop('camera', None), args.bins)
        else:
            camera = np.clip(actions.pop('camera', None), -10, 10)
        actions['pitch'] = camera[:, 0]
        actions['yaw'] = camera[:, 1]
        # Priority: nearbySmelt, nearbyCraft, craft, equip, place
        if 'craft' in actions:
            cls.craft_labels(actions)


def discretise_camera(camera, bins):
    """
    :param angle_vector: n x 2 numpy array
    :return: n x 5 one hot numpy array
    """
    # camera_discrete = np.zeros(camera.shape)
    # for axis in [0, 1]:
    #     camera_discrete[camera[:, axis] < - 2 , axis] =  0 # left/down
    #     camera_discrete[camera[:, axis] <  ,axis] =  1 # None
    #     camera_discrete[camera[:, axis] == 0 ,axis] =  2 # Up/right
    #     camera_discrete[camera[:, axis] > 0 ,axis] =  2 # Up/right
    #     camera_discrete[camera[:, axis] > 0 ,axis] =  2 # Up/right
    # bins_ = np.append(-np.flip(bins)[:-1], bins)
    bins_ = -np.flip(bins)
    # print(bins_)

    disc_cam_data = np.zeros(camera.shape, dtype=np.uint8)

    for index, (bin_prev, bin) in enumerate(zip(bins_[:-1], bins_[1:])):
        disc_cam_data[(camera >= bin_prev) & (camera < bin)] = index
    for index, (bin_prev, bin) in enumerate(zip(bins[:-1], bins[1:]), start=len(bins_)):
        disc_cam_data[(camera > bin_prev) & (camera <= bin)] = index

    disc_cam_data[camera < bins_[0]] = 0
    disc_cam_data[camera > bins[-1]] = (len(bins) - 1) * 2
    disc_cam_data[camera == bins[0]] = len(bins) - 1

    # for index in range(len(bins)-1):
    #     disc_cam_data[(-camera >= bins[index]) & (-camera < bins[index+1])] = (len(bins) - 1) - index
    #
    # for index in range(len(bins)-1):
    #     disc_cam_data[(camera >= bins[index]) & (camera < bins[index+1])] = index + (len(bins) + 1)

    # disc_cam_data[camera == bins[0]] = len(bins) - 1

    # disc_cam_data[camera > bins[-1]] = len(bins) - 1
    # for i in range(1, len(bins) // 2):
    #     disc_cam_data[(bins[i] <= camera) & (camera < bins[i + 1])] = i
    # for i in range(len(bins) // 2, len(bins) - 1):
    #     disc_cam_data[(bins[i] < camera) & (camera <= bins[i + 1])] = i + 1

    return disc_cam_data


def combine_actions_discrete(action_space, args: Arguments):
    ben_len = len(args.bins) * 2 - 1
    if 'craft' in action_space.spaces:
        return Dict(OrderedDict({
            'pitch': Discrete(ben_len),
            'yaw': Discrete(ben_len),
            'act': Discrete(8),
            'item': Discrete(27)  # All crafting actions
        }))
    else:
        return Dict(OrderedDict({
            'pitch': Discrete(ben_len),
            'yaw': Discrete(ben_len),
            'act': Discrete(8)
        }))


class CombinedActionsDiscrete(gym.ActionWrapper):
    def __init__(self, env, args: Arguments):
        super().__init__(env)
        self.act_id = [
            (('attack', 0),),
            (('attack', 1), ('forward', 1), ('jump', 1)),
            (('attack', 1), ('forward', 1)),
            (('forward', 1), ('jump', 1)),
            (('attack', 1), ('jump', 1)),
            (('attack', 1),),
            (('forward', 1),),
            (('jump', 1),)
        ]  # lengte 8
        self.item_id = [
            *zip(repeat('place'), range(7)),
            *zip(repeat('equip'), range(1, 8)),
            *zip(repeat('craft'), range(1, 5)),
            *zip(repeat('nearbyCraft'), range(1, 8)),
            *zip(repeat('nearbySmelt'), range(1, 3)),
        ]  # lengte 27

        self.camera_id = np.append(-np.flip(args.bins)[:-1], args.bins)

        self.action_space = combine_actions_discrete(self.env.action_space, args)

    def action(self, action):
        no_op = self.env.action_space.no_op()
        pitch = action['pitch']
        yaw = action['yaw']
        if isinstance(pitch, np.ndarray):
            pitch = pitch[0]
        if isinstance(yaw, np.ndarray):
            yaw = yaw[0]
        no_op['camera'] = np.array([self.camera_id[pitch], self.camera_id[yaw]])
        act = action['act']
        for move_key, move_value in self.act_id[act]:
            no_op[move_key] = move_value
        if 'item' in action:
            items = action['item']
            item_key, item_value = self.item_id[items]
            no_op[item_key] = item_value
        return no_op


def combine_actions_cont(action_space):
    if 'craft' in action_space.spaces:
        return Dict(OrderedDict({
            'pitch': Box(low=-10, high=10, shape=(1,)),
            'yaw': Box(low=-10, high=10, shape=(1,)),
            'act': Discrete(8),
            'item': Discrete(27)  # All crafting actions
        }))
    else:
        return Dict(OrderedDict({
            'pitch': Box(low=-10, high=10, shape=(1,)),
            'yaw': Box(low=-10, high=10, shape=(1,)),
            'act': Discrete(8)
        }))


class CombinedActionsCont(gym.ActionWrapper):
    def __init__(self, env):
        super().__init__(env)
        self.act_id = [
            (('attack', 0),),
            (('attack', 1), ('forward', 1), ('jump', 1)),
            (('attack', 1), ('forward', 1)),
            (('forward', 1), ('jump', 1)),
            (('attack', 1), ('jump', 1)),
            (('attack', 1),),
            (('forward', 1),),
            (('jump', 1),)
        ]  # lengte 8
        self.item_id = [
            *zip(repeat('place'), range(7)),
            *zip(repeat('equip'), range(1, 8)),
            *zip(repeat('craft'), range(1, 5)),
            *zip(repeat('nearbyCraft'), range(1, 8)),
            *zip(repeat('nearbySmelt'), range(1, 3)),
        ]  # lengte 27

        self.action_space = combine_actions_cont(self.env.action_space)

        # Branch for crafting (all crafts)
        # Camera pitch - continuous
        # Camera yaw - continuous
        # Button branch - None, f, a, j, fa, aj, fj, faj
        # [Excluded] - left, right, back, sprint, sneak

        # print(Dict(self.wrapping_action_space))
        # print(env.action_space.noop())

    def action(self, action):
        no_op = self.env.action_space.no_op()
        pitch = action['pitch']
        yaw = action['yaw']
        if isinstance(pitch, np.ndarray):
            pitch = pitch[0]
        if isinstance(yaw, np.ndarray):
            yaw = yaw[0]
        no_op['camera'] = np.array([pitch, yaw])

        act = action['act']
        for move_key, move_value in self.act_id[act]:
            no_op[move_key] = move_value
        if 'item' in action:
            items = action['item']
            item_key, item_value = self.item_id[items]
            no_op[item_key] = item_value
        return no_op


def actions_(action_space, args: Arguments):
    bin_len = len(args.bins) * 2
    if 'craft' in action_space.spaces:
        return Dict(OrderedDict({
            'action': Discrete(bin_len * 2 + 3 + 27),
        }))
    else:
        return Dict(OrderedDict({
            'action': Discrete(bin_len * 2 + 3),
        }))


class ActionSpace_(gym.ActionWrapper):
    def __init__(self, env, args: Arguments):
        super().__init__(env)
        camera_id = np.append(-np.flip(args.bins), args.bins)
        print(camera_id)

        self.action_list = [*zip(repeat('pitch'), camera_id),
                            *zip(repeat('yaw'), camera_id),
                            ('attack', 1),
                            ('forward', 1),
                            ('jump', 1)]

        self.action_space = actions_(self.env.action_space, args)

    def action(self, action):
        no_op = self.env.action_space.no_op()

        action_label, action_value = self.action_list[action['action']]

        if action_label == 'pitch':
            no_op['camera'] = np.array([action_value, 0])
        elif action_label == 'yaw':
            no_op['camera'] = np.array([0, action_value])
        else:
            no_op[action_label] = action_value
        no_op['attack'] = 1
        if 'Obtain' in self.env.unwrapped.spec.id:
            items = action['item']
            item_key, item_value = self.item_id[items]
            no_op[item_key] = item_value

        return no_op


def branched_actions(action_space, args: Arguments):
    bin_len = len(args.bins) * 2 - 1
    if 'craft' in action_space.spaces:
        return Dict(OrderedDict({
            'pitch': Discrete(bin_len),
            'yaw': Discrete(bin_len),
            'attack': Discrete(2),
            'forward': Discrete(2),
            'jump': Discrete(2),
            'item': Discrete(27)  # All crafting actions
        }))
    else:
        return Dict(OrderedDict({
            'pitch': Discrete(bin_len),
            'yaw': Discrete(bin_len),
            'attack': Discrete(2),
            'forward': Discrete(2),
            'jump': Discrete(2),
            'item': Discrete(27)  # haal weer later uit!!!
        }))


class BranchedActionSpace(gym.ActionWrapper):
    def __init__(self, env, args: Arguments):
        super().__init__(env)

        self.item_id = [
            *zip(repeat('place'), range(7)),
            *zip(repeat('equip'), range(1, 8)),
            *zip(repeat('craft'), range(1, 5)),
            *zip(repeat('nearbyCraft'), range(1, 8)),
            *zip(repeat('nearbySmelt'), range(1, 3)),
        ]
        self.camera_id = np.append(-np.flip(args.bins)[:-1], args.bins)
        print(self.camera_id)
        self.action_space = branched_actions(self.env.action_space, args)

    def action(self, action):
        no_op = self.env.action_space.no_op()
        # print(no_op)
        # print(self.env.unwrapped.spec.id)
        pitch = action['pitch']
        yaw = action['yaw']
        if isinstance(pitch, np.ndarray):
            pitch = pitch[0]
        if isinstance(yaw, np.ndarray):
            yaw = yaw[0]
        no_op['camera'] = np.array([self.camera_id[pitch], self.camera_id[yaw]])
        # print(no_op['camera'])
        no_op['attack'] = action['attack']
        no_op['forward'] = action['forward']
        no_op['jump'] = action['jump']

        if 'Obtain' in self.env.unwrapped.spec.id:
            items = action['item']
            item_key, item_value = self.item_id[items]
            no_op[item_key] = item_value
        return no_op


def exclude_actions(action_space, actions: list = None):
    # wrapping_action_space = copy.deepcopy(action_space.spaces)
    if actions:
        for key in actions:
            action_space.pop(key)


class ExcludeActions(gym.ActionWrapper):
    """Convert MineRL env's `Dict` action space as a serial discrete action space.
    The term "serial" means that this wrapper can only push one key at each step.
    "attack" action will be alwarys triggered.
    """

    def __init__(self, env, excluded_actions: list = None):
        super().__init__(env)
        self.ex_action = self.env.action_space.no_op()
        self.action_space = exclude_actions(self.env.action_space, excluded_actions)

    def action(self, action):
        self.ex_action.update(action)
        return self.ex_action


class OneHotActions(gym.ActionWrapper):
    """Convert MineRL env's `Dict` action space as a serial discrete action space.
    The term "serial" means that this wrapper can only push one key at each step.
    "attack" action will be alwarys triggered.
    """
    ONE_HOT_ACTIONS_LIST = ['place', 'equip', 'craft', 'nearbyCraft', 'nearbySmelt']
    BINARY_KEYS = ['forward', 'back', 'left', 'right', 'jump', 'sneak', 'sprint', 'attack']
    CRAFT_OPTIONS = ['none', 'torch', 'stick', 'planks', 'crafting_table']
    EQUIP_OPTIONS = ['none', 'air', 'wooden_axe', 'wooden_pickaxe', 'stone_axe',
                     'stone_pickaxe', 'iron_axe', 'iron_pickaxe']
    NEARBY_CRAFT_OPTIONS = ['none', 'wooden_axe', 'wooden_pickaxe', 'stone_axe',
                            'stone_pickaxe', 'iron_axe', 'iron_pickaxe', 'furnace']
    NEARBY_SMELT_OPTIONS = ['none', 'iron_ingot', 'coal']
    PLACE_OPTIONS = ['none', 'dirt', 'stone', 'cobblestone', 'crafting_table', 'furnace', 'torch']

    def __init__(self, env, exclude_noop=False):
        super().__init__(env)
        self.exclude_noop = exclude_noop

        self.wrapping_action_space = copy.deepcopy(self.env.action_space.spaces)
        self.one_hot_action_lists = OrderedDict([
            # 'none', 'dirt' (Obtain*:)+ 'stone', 'cobblestone', 'crafting_table', 'furnace', 'torch'
            ('place', self.PLACE_OPTIONS),
            ('equip', self.EQUIP_OPTIONS),
            ('craft', self.CRAFT_OPTIONS),
            ('nearbyCraft', self.NEARBY_CRAFT_OPTIONS),
            ('nearbySmelt', self.NEARBY_SMELT_OPTIONS),
        ])
        self.one_hot_actions = OrderedDict([
            # 'none', 'dirt' (Obtain*:)+ 'stone', 'cobblestone', 'crafting_table', 'furnace', 'torch'
            ('place', Discrete(len(self.PLACE_OPTIONS))),  # Discrete(7)  e [0, 7]
            ('equip', Discrete(len(self.EQUIP_OPTIONS))),
            ('craft', Discrete(len(self.CRAFT_OPTIONS))),
            ('nearbyCraft', Discrete(len(self.NEARBY_CRAFT_OPTIONS))),
            ('nearbySmelt', Discrete(len(self.NEARBY_SMELT_OPTIONS))),
        ])
        for key, value in self.one_hot_actions.items():
            if key in self.wrapping_action_space.keys():
                self.wrapping_action_space[key] = value
        self.wrapping_action_space.move_to_end('place')
        self.wrapping_action_space.move_to_end('equip')
        self.wrapping_action_space.move_to_end('craft')
        self.wrapping_action_space.move_to_end('nearbyCraft')
        self.wrapping_action_space.move_to_end('nearbySmelt')
        self.action_space = minerl.env.spaces.Dict(self.wrapping_action_space)

    def action(self, action):
        action = copy.copy(action)
        for key in self.one_hot_actions.keys():
            if key in action.keys():
                action[key] = self.one_hot_action_lists[key][action[key]]
        return action


if __name__ == '__main__':
    env = gym.make('MineRLObtainDiamond-v0')
    env = CombinedActionsCont(env)
    # env.step("action")
