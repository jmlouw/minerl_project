from gym.envs.registration import register
from minerl.env import make_navigate_text, navigate_action_space, navigate_observation_space
import os

cwd = os.getcwd()
my_mission_dir = '/'.join([cwd, 'vision', 'custom_missions'])

register(
    id='MineRLNavigateDense-v9',
    entry_point='minerl.env:MineRLEnv',
    kwargs={
        'xml': os.path.join(my_mission_dir, 'navigationDenseDepth.xml'),
        'observation_space': navigate_observation_space,
        'action_space': navigate_action_space,
        'docstr': make_navigate_text('normal', True)
    },
    max_episode_steps=6000,
)