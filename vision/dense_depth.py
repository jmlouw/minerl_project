"""
Taken from DenseDepth PyTorch version
https://github.com/ialhashim/DenseDepth/blob/master/PyTorch/model.py
"""

import torch
import torch.nn as nn
from torchvision.transforms import Compose, ToTensor, ToPILImage, Lambda
import torch.nn.functional as F
import numpy as np
from math import exp
from collections import OrderedDict

from utils.gen_args import Arguments


class UpSample(nn.Sequential):
    def __init__(self, skip_input, output_features):
        super(UpSample, self).__init__()
        self.convA = nn.Conv2d(skip_input, output_features, kernel_size=3, stride=1, padding=1)
        self.leakyreluA = nn.LeakyReLU(0.2)
        self.convB = nn.Conv2d(output_features, output_features, kernel_size=3, stride=1, padding=1)
        self.leakyreluB = nn.LeakyReLU(0.2)

    def forward(self, x, concat_with):
        up_x = F.interpolate(x, size=[concat_with.size(2), concat_with.size(3)], mode='bilinear', align_corners=True)
        return self.leakyreluB(self.convB(self.leakyreluA(self.convA(torch.cat([up_x, concat_with], dim=1)))))


class Decoder(nn.Module):
    def __init__(self, num_features=2208, decoder_width=0.5):
        super(Decoder, self).__init__()
        features = int(num_features * decoder_width)

        self.conv2 = nn.Conv2d(num_features, features, kernel_size=1, stride=1, padding=1)

        self.up1 = UpSample(skip_input=features//1 + 384, output_features=features//2)
        self.up2 = UpSample(skip_input=features//2 + 192, output_features=features//4)
        self.up3 = UpSample(skip_input=features//4 + 96, output_features=features//8)
        self.up4 = UpSample(skip_input=features//8 + 96, output_features=features//16)
        self.up5 = UpSample(skip_input=features//16 + 11, output_features=features//16)

        self.conv3 = nn.Conv2d(features//16, 1, kernel_size=3, stride=1, padding=1)
        self.activation = nn.Sigmoid()

        self.in_conv = nn.Sequential(OrderedDict([
            ('conv', nn.Conv2d(3, 11, kernel_size=3, stride=1, padding=1, bias=False)),
            ('norm', nn.BatchNorm2d(11)),
            ('relu', nn.ReLU(inplace=True))
        ]))


    def forward(self, features):
        in_block, x_block0, x_block1, x_block2, x_block3, x_block4 = features[0], features[3], features[4], features[6], features[8], features[11]
        x_d0 = self.conv2(x_block4)
        x_d1 = self.up1(x_d0, x_block3)
        x_d2 = self.up2(x_d1, x_block2)
        x_d3 = self.up3(x_d2, x_block1)
        x_d4 = self.up4(x_d3, x_block0)
        x_d5 = self.up5(x_d4, self.in_conv(in_block))
        return self.activation(self.conv3(x_d5))


class Encoder(nn.Module):
    def __init__(self):
        super(Encoder, self).__init__()
        import torchvision.models as models
        self.original_model = models.densenet161(pretrained=False)

    def forward(self, x):
        features = [x]
        for k, v in self.original_model.features._modules.items():
            features.append(v(features[-1]))
        return features


class DenseDepth(nn.Module):
    def __init__(self, args: Arguments):
        super(DenseDepth, self).__init__()
        self.encoder = Encoder()
        self.decoder = Decoder()
        self.args = args

        self.make_tensor = Compose([
            Lambda(lambda np_batch: [ToPILImage()(img) for img in np_batch]),
            Lambda(lambda pics: torch.stack([ToTensor()(pic) for pic in pics]))
        ])

    def forward(self, x):
        if not isinstance(x, torch.Tensor):
            x = self.make_tensor(x).to(self.args.device)
            x = x.view(-1, 3, 64, 64)
        x = self.encoder(x)
        return self.decoder(x)


def gaussian(window_size, sigma):
    gauss = torch.Tensor([exp(-(x - window_size//2)**2/float(2*sigma**2)) for x in range(window_size)])
    return gauss/gauss.sum()


def create_window(window_size, channel=1):
    _1D_window = gaussian(window_size, 1.5).unsqueeze(1)
    _2D_window = _1D_window.mm(_1D_window.t()).float().unsqueeze(0).unsqueeze(0)
    window = _2D_window.expand(channel, 1, window_size, window_size).contiguous()
    return window


def ssim(img1, img2, val_range, window_size=11, window=None, size_average=True, full=False):
    L = val_range

    padd = 0
    (_, channel, height, width) = img1.size()
    if window is None:
        real_size = min(window_size, height, width)
        window = create_window(real_size, channel=channel).to(img1.device)

    mu1 = F.conv2d(img1, window, padding=padd, groups=channel)
    mu2 = F.conv2d(img2, window, padding=padd, groups=channel)

    mu1_sq = mu1.pow(2)
    mu2_sq = mu2.pow(2)
    mu1_mu2 = mu1 * mu2

    sigma1_sq = F.conv2d(img1 * img1, window, padding=padd, groups=channel) - mu1_sq
    sigma2_sq = F.conv2d(img2 * img2, window, padding=padd, groups=channel) - mu2_sq
    sigma12 = F.conv2d(img1 * img2, window, padding=padd, groups=channel) - mu1_mu2

    C1 = (0.01 * L) ** 2
    C2 = (0.03 * L) ** 2

    v1 = 2.0 * sigma12 + C2
    v2 = sigma1_sq + sigma2_sq + C2
    cs = torch.mean(v1 / v2)  # contrast sensitivity

    ssim_map = ((2 * mu1_mu2 + C1) * v1) / ((mu1_sq + mu2_sq + C1) * v2)

    if size_average:
        ret = ssim_map.mean()
    else:
        ret = ssim_map.mean(1).mean(1).mean(1)

    if full:
        return ret, cs

    return ret


def calc_accuracy(pred: torch.tensor, target: torch.tensor):
    pred = pred.view(-1)
    target = target.view(-1)
    data_points = len(pred)
    pred = pred.round()
    correct = len(pred[pred == target])/data_points
    delta = torch.abs(pred[pred != target] - target[pred != target])
    av_delta = torch.mean(delta)
    std_delta = torch.std(delta)
    return correct, av_delta, std_delta

