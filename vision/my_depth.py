import torch
from torch import nn, optim
import numpy as np
from torchvision.transforms import Lambda, ToPILImage, ToTensor, Compose

# User imports
from utils.gen_args import Arguments
from vision.dense_depth import DenseDepth, ssim
from utils.utility_functions import LinearBlock, CNNBlock, OneDto3D, \
    TransposeCNNBlock, ChainConv2d, ChainDeConv2d, ChainLinear, Flatten, print_b
from vision.dense_depth import calc_accuracy


class MyDepthModel(nn.Module):
    def __init__(self,
                 args: Arguments,
                 img_size: tuple = (64, 64),
                 in_channels: int = 3,
                 loss_func: nn.Module = nn.SmoothL1Loss,
                 activation: nn.Module = nn.ReLU,
                 optimizer: optim = optim.Adam):
        super(MyDepthModel, self).__init__()
        self.args = args
        self.img_size = img_size
        self.in_channels = in_channels
        self.activation = activation
        self.layers = self._manual_gen_model()
        # self.model = DenseDepth(args=args)

        self.to(args.device)
        self.optimizer = optimizer(params=self.layers.parameters(), lr=args.lr)
        self.loss_func = loss_func()
        self.obs_batch = None
        self.make_tensor = Compose([
            Lambda(lambda np_batch: [ToPILImage()(img) for img in np_batch]),
            Lambda(lambda pics: torch.stack([ToTensor()(pic) for pic in pics]))
        ])

    def forward(self, x):
        for i, layer in enumerate(self.layers):
            x = layer(x)
        # x = self.model(x)
        return x.mul(255.0)

    def _generate_architecture(self):
        conv1 = CNNBlock(
            in_channels=self.in_channels,
            out_channels=8,
            kernel_size=4,
            stride=2,
            in_height=self.img_size[0],
            in_width=self.img_size[1]
        )
        conv2 = CNNBlock(
            out_channels=8**2,
            kernel_size=3,
            stride=2,
            prev_layer=conv1
        )
        conv3 = CNNBlock(
            out_channels=8**3,
            kernel_size=3,
            stride=2,
            prev_layer=conv2
        )
        layer_list = [conv1, conv2, conv3]
        for i in range(4):
            layer_list.append(
                LinearBlock(
                    out_features=int(conv3.out_height*conv3.out_width * 8**3 * 1.25**(i+1)),
                    prev_layer=layer_list[-1]
                )
            )

        layers = nn.ModuleList(layer_list)
        return layers

    def _manual_gen_model(self):
        layer_list = [
            nn.Conv2d(
                in_channels=self.in_channels,
                out_channels=8,
                kernel_size=4,
                stride=2
            ),
            nn.BatchNorm2d(num_features=8),
            self.activation()
        ]
        for i in range(2):
            layer_list.extend((
                nn.Conv2d(
                    in_channels=2**(i+3),
                    out_channels=2**(i+4),
                    kernel_size=3,
                    stride=2
                ),
                nn.BatchNorm2d(num_features=2**(i+4)),
                self.activation()
            ))

        layer_list.extend((
            Flatten(),
            nn.Linear(
                in_features=2 ** 5 * 7 ** 2,
                out_features=int(2 ** 5 * 7 ** 2 * 1.25)
            ),
            nn.BatchNorm1d(num_features=int(2 ** 5 * 7 ** 2 * 1.25)),
            self.activation()
        ))
        for i in range(3):
            layer_list.extend((
                nn.Linear(
                    in_features=int(2 ** 5 * 7 ** 2 * 1.25**(i+1)),
                    out_features=int(2 ** 5 * 7 ** 2 * 1.25**(i+2))
                ),
                nn.BatchNorm1d(num_features=int(2 ** 5 * 7 ** 2 * 1.25**(i+2))),
                self.activation()
            ))
        for i in reversed(range(4)):
            layer_list.extend((
                nn.Linear(
                    in_features=int(2 ** 5 * 7 ** 2 * 1.25**(i+1)),
                    out_features=int(2 ** 5 * 7 ** 2 * 1.25**i)
                ),
                nn.BatchNorm1d(num_features=int(2 ** 5 * 7 ** 2 * 1.25**i)),
                self.activation()
            ))
        layer_list.append(OneDto3D(height=7, width=7))
        for i in reversed(range(2)):
            layer_list.extend((
                nn.ConvTranspose2d(
                    in_channels=2**(i+4),
                    out_channels=2**(i+3),
                    kernel_size=3,
                    stride=2
                ),
                nn.BatchNorm2d(num_features=2**(i+3)),
                self.activation()
            ))
        layer_list.extend((
            nn.ConvTranspose2d(
                in_channels=8,
                out_channels=1,
                kernel_size=4,
                stride=2
            ),
            # self.activation()
            nn.Sigmoid()
        ))
        return nn.ModuleList(layer_list)

    def train_model(self, obs: np.ndarray):
        self.obs_batch = np.concatenate((self.obs_batch, obs[np.newaxis]), axis=0) if self.obs_batch is not None else \
            obs[np.newaxis]
        if len(self.obs_batch) == self.args.batch_size:
            self.optimizer.zero_grad()
            imgs = self.obs_batch[..., :3].copy()
            depths = self.obs_batch[..., -1][:, np.newaxis].copy()
            self.obs_batch = None

            imgs = self.make_tensor(imgs).to(self.args.device)
            depths = torch.tensor(depths).float().to(self.args.device, non_blocking=True)

            prediction = self(imgs)

            l_depth = self.loss_func(prediction, depths)
            l_ssim = torch.clamp(
                (1 - ssim(
                    prediction,
                    depths,
                    val_range=prediction.max().item()-prediction.min().item())
                 ) * 0.5, 0, 1
            )

            loss = (1.0 * l_ssim) + (0.1 * l_depth)

            loss.backward()
            self.optimizer.step()
            correct, av_delta, std_delta = calc_accuracy(pred=prediction, target=depths)
            return correct, av_delta, std_delta
        return None
